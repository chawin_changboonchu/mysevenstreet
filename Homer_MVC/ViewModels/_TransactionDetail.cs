﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.ViewModels
{
    public class _TransactionDetail : Models.Transaction
    {
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string UserName { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
    }
}
USE [SeventhStreet]
GO

/****** Object:  Table [dbo].[Transaction]    Script Date: 5/27/2017 8:31:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Transaction](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionNumber] [nvarchar](max) NULL,
	[ProductID] [int] NULL,
	[AmountXS] [int] NULL,
	[AmountS] [int] NULL,
	[AmountM] [int] NULL,
	[AmountL] [int] NULL,
	[AmountXL] [int] NULL,
	[AmountXXL] [int] NULL,
	[UserID] [int] NULL,
	[Date] [datetime] NULL,
	[FromBranchID] [int] NULL,
	[SupplierID] [int] NULL,
	[ToBranchID] [int] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.Models
{
    public class _OrderStatus
    {
        public string OrderNumber { get; set; }
        public int? OrderUserID { get; set; }
        public string DeliveryName { get; set; }
        public string DeliveryAddress { get; set; }
        public string Remark { get; set; }
        public int? Status { get; set; }
        public DateTime? OrderDate { get; set; }
        public string UserName { get; set; }
        public string ShopName { get; set; }
    }
}
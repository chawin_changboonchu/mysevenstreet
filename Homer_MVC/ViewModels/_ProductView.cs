﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.Models
{
    public class _ProductView : Product
    {
        private string _sortId;
        public string ProductSortID
        {
            get
            {
                return base.SortID.Value.ToString("D3");
            }
        }
        public string CategoryName { get; set; }
        public string BranchName { get; set; }
    }
}
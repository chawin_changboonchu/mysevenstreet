﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.Models
{
    public class _MinimumStock
    {
        public Product Product { get; set; }
        public Branch Branch { get; set; }
        public Category Category { get; set; }
        public StockSetting StockSetting { get; set; }
    }
}
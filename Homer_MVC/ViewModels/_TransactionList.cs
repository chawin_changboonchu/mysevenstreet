﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.ViewModels
{
    public class _TransactionList
    {
        public List<_TransactionDetail> transactions { get; set; }
        public List<Models.Product> products { get; set; }
    }
}
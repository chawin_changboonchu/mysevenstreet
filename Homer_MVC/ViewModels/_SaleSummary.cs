﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.Models
{
    public class _SaleSummary
    {
        public string BranchName { get; set; }
        public string Category { get; set; }
        public List<_MonthReport> MonthReports { get; set; }
        public _MonthReport GrandTotal { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.Models
{
    public class _MonthReport
    {
        public string MonthName { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.ViewModels
{
    public class _OrderDetail : Models.Order
    {
        public List<Models.Product> Products { get; set; }
        public int UserType { get; set; }
    }
}
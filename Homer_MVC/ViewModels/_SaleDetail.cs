﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MySeventhStreet.Models
{
    public class _SaleDetail
    {
        public int? OrderUserID { get; set; }
        public string OrderNumber { get; set; }
        public DateTime? OrderDate { get; set; }
        public string BranchName { get; set; }
        public string UserName { get; set; }
        public string Category { get; set; }
        public string Barcode { get; set; }
        public int? ProductID { get; set; }
        public string ProductName { get; set; }
        public int? ProductPrice { get; set; }
        public int? Quantity { get; set; }
        public int? Discount { get; set; }
        public int? TotalPrice { get; set; }
    }
}
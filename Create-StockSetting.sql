USE [SeventhStreet]
GO

/****** Object:  Table [dbo].[StockSetting]    Script Date: 10/2/2017 12:18:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StockSetting](
	[ProductID] [int] NOT NULL,
	[BranchID] [int] NOT NULL,
	[MinXS] [int] NULL,
	[MinS] [int] NULL,
	[MinM] [int] NULL,
	[MinL] [int] NULL,
	[MinXL] [int] NULL,
	[MinXXL] [int] NULL,
 CONSTRAINT [PK_StockSetting] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC,
	[BranchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MySeventhStreet.Helpers;
using MySeventhStreet.Models;
using MySeventhStreet.ViewModels;
using System.Web;
using OfficeOpenXml;
using System.IO;
using System.Security;
using OfficeOpenXml.Style;

namespace MySeventhStreet.Controllers
{
    public class MainController : Controller
    {
        private readonly SeventhStreetEntities entities = new SeventhStreetEntities();
        private List<Product> shoppingCartList = new List<Product>();

        [Authorize]
        [HttpPost]
        public ActionResult AddShoppingCart(string id, string AmountXS, string AmountS, string AmountM, string AmountL, string AmountXL,
            string AmountF, int Branch)
        {
            if (!string.IsNullOrEmpty(id))
            {
                int dummy;
                var tokenId = id.Split('_');
                var productId = int.Parse(tokenId[0]);
                var product = entities.Product.FirstOrDefault(p => p.ProductID == productId);
                var amountXS = !int.TryParse(AmountXS, out dummy) ? 0 : int.Parse(AmountXS);
                var amountS = !int.TryParse(AmountS, out dummy) ? 0 : int.Parse(AmountS);
                var amountM = !int.TryParse(AmountM, out dummy) ? 0 : int.Parse(AmountM);
                var amountL = !int.TryParse(AmountL, out dummy) ? 0 : int.Parse(AmountL);
                var amountXL = !int.TryParse(AmountXL, out dummy) ? 0 : int.Parse(AmountXL);
                var amountF = !int.TryParse(AmountF, out dummy) ? 0 : int.Parse(AmountF);
                var shopItems = Session["ShoppingCart"] as List<Product>;
                if (shopItems == null)
                {
                    shopItems = new List<Product>();
                }

                if (product != null)
                {
                    shopItems.Add(new Product
                    {
                        ProductID = productId,
                        Name = product.Name,
                        AmountXS = amountXS,
                        AmountS = amountS,
                        AmountM = amountM,
                        AmountL = amountL,
                        AmountXL = amountXL,
                        AmountF = amountF,
                        CategoryID = product.CategoryID,
                        Price = product.Price
                    });

                    Session["ShoppingCart"] = shopItems;
                }
            }

            UpdateShoppingCart();

            return Json(Url.Action("OrderProduct", "Main"));
        }

        private int CountItem(List<Product> products)
        {
            int total = 0;
            if (products != null)
            {
                foreach (var product in products)
                {
                    total += product.AmountS ?? 0;
                    total += product.AmountM ?? 0;
                    total += product.AmountXS ?? 0;
                    total += product.AmountL ?? 0;
                    total += product.AmountXL ?? 0;
                    total += product.AmountF ?? 0;
                }
            }

            return total;
        }

        [Authorize]
        [HttpPost]
        public ActionResult AdminAddShoppingCart(string id, string AmountXS, string AmountS, string AmountM, string AmountL, string AmountXL,
    string AmountF, int Branch)
        {
            if (!string.IsNullOrEmpty(id))
            {
                int dummy;
                var tokenId = id.Split('_');
                var productId = int.Parse(tokenId[0]);
                var product = entities.Product.FirstOrDefault(p => p.ProductID == productId);
                var amountXS = !int.TryParse(AmountXS, out dummy) ? 0 : int.Parse(AmountXS);
                var amountS = !int.TryParse(AmountS, out dummy) ? 0 : int.Parse(AmountS);
                var amountM = !int.TryParse(AmountM, out dummy) ? 0 : int.Parse(AmountM);
                var amountL = !int.TryParse(AmountL, out dummy) ? 0 : int.Parse(AmountL);
                var amountXL = !int.TryParse(AmountXL, out dummy) ? 0 : int.Parse(AmountXL);
                var amountF = !int.TryParse(AmountF, out dummy) ? 0 : int.Parse(AmountF);
                var shopItems = Session["ShoppingCart"] as List<Product>;
                if (shopItems == null)
                {
                    shopItems = new List<Product>();
                }

                if (product != null)
                {
                    shopItems.Add(new Product
                    {
                        ProductID = productId,
                        Name = product.Name,
                        AmountXS = amountXS,
                        AmountS = amountS,
                        AmountM = amountM,
                        AmountL = amountL,
                        AmountXL = amountXL,
                        AmountF = amountF,
                        CategoryID = product.CategoryID,
                        Price = product.Price
                    });
                    Session["ShoppingCart"] = shopItems;
                }
            }

            UpdateShoppingCart();

            return Json(Url.Action("AdminOrderProduct", "Main"));
        }

        private void UpdateShoppingCart()
        {
            var shopItems = Session["ShoppingCart"] as List<Product>;
            if (shopItems == null) return;
            var totalCartItemCount = CountItem(shopItems);
            var userType = 0;
            if (Session["impersonateUserType"] != null)
            {
                userType = int.Parse(Session["impersonateUserType"].ToString());
            }
            else if (Session["UserType"] != null)
            {
                userType = int.Parse(Session["UserType"].ToString());
            }

            var uniqueProductIDList = shopItems.Select(i => i.ProductID).Distinct().ToList();

            foreach (var uniqueProductID in uniqueProductIDList)
            {
                if (shopItems.Count(i => i.ProductID == uniqueProductID) > 1)
                {
                    var toCombineItemList = shopItems.Where(i => i.ProductID == uniqueProductID).ToList();

                    var toAddProduct = new Product()
                    {
                        AmountF = 0,
                        AmountL = 0,
                        AmountM = 0,
                        AmountS = 0,
                        AmountXL = 0,
                        AmountXS = 0
                    };

                    foreach (var item in toCombineItemList)
                    {
                        toAddProduct.ProductID = item.ProductID;
                        toAddProduct.Name = item.Name;
                        toAddProduct.AmountXS += item.AmountXS;
                        toAddProduct.AmountS += item.AmountS;
                        toAddProduct.AmountM += item.AmountM;
                        toAddProduct.AmountL += item.AmountL;
                        toAddProduct.AmountXL += item.AmountXL;
                        toAddProduct.AmountF += item.AmountF;
                        toAddProduct.CategoryID = item.CategoryID;
                        toAddProduct.Price += item.Price;

                        shopItems.Remove(item);
                    }

                    shopItems.Add(toAddProduct);
                }
            }

            foreach (var product in shopItems)
            {
                var basedProduct = entities.Product.FirstOrDefault(p => p.ProductID == product.ProductID);
                var productCategory = entities.Category.FirstOrDefault(c => c.CategoryID == product.CategoryID);
                var totalAmount = (product.AmountXS ?? 0) + (product.AmountS ?? 0) + (product.AmountM ?? 0) + (product.AmountL ?? 0) + (product.AmountXL ?? 0) + (product.AmountF ?? 0);
                var productPrice = 0;
                var totalItemPrice = 0;

                if ((product.Name.Contains("แขนสั้น") || productCategory.Name.Contains("แขนสั้น") || (productCategory.Description != null && productCategory.Description.Contains("แขนสั้น")) || productCategory.SortID == 1) && userType == 4 && totalCartItemCount < 50)
                {
                    productPrice = 130;
                }
                else if (userType == 2)
                {
                    productPrice = basedProduct.Price ?? 0;
                }
                else if (userType == 5)
                {
                    productPrice = basedProduct.Price4 ?? 0;
                }
                else if (userType == 6)
                {
                    productPrice = basedProduct.PriceVIP ?? 0;
                }
                else
                {
                    //if (totalCartItemCount >= 50)
                    //{
                    productPrice = basedProduct.Price3 ?? 0;
                    //}
                    //else
                    //{
                    //    productPrice = basedProduct.Price2 ?? 0;
                    //}
                }

                totalItemPrice = productPrice * totalAmount;

                product.Price = totalItemPrice;
            }

            Session["ShoppingCart"] = shopItems;
        }

        [Authorize]
        public ActionResult AdminBranch()
        {
            var braches = entities.Branch.ToList();

            return View(braches);
        }

        [Authorize]
        public ActionResult AdminOrder()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }   

            var userId = int.Parse(Session["UserID"].ToString());
            var user = entities.User.FirstOrDefault(u => u.UserID == userId);
            var order = entities.Order.Where(o => o.OrderDateTime.Value.Month == DateTime.Now.Month && o.OrderDateTime.Value.Year == DateTime.Now.Year).GroupBy(o => o.OrderNumber).Select(o => new _OrderStatus
            {
                UserName = entities.User.FirstOrDefault(u => u.UserID == o.FirstOrDefault().UserID).UserName,
                OrderUserID = o.FirstOrDefault().OrderUserID.Value,
                ShopName = entities.User.FirstOrDefault(u => u.UserID == o.FirstOrDefault().UserID).ShopName,
                OrderNumber = o.Key,
                DeliveryAddress = o.FirstOrDefault().DeliveryAddress,
                Remark = o.FirstOrDefault().Remark,
                DeliveryName = o.FirstOrDefault().DeliveryName,
                OrderDate = o.FirstOrDefault().OrderDateTime,
                Status = o.FirstOrDefault().Status
            });

            return View(order.ToList());
        }

        [Authorize]
        public ActionResult AdminSaleSummary()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var startDate = DateTime.Now.AddDays(-2);
            var endDate = DateTime.Now;

            var filterOrder = entities.Order.Where(o => o.OrderDateTime.Value >= startDate && o.OrderDateTime.Value <= endDate).ToList();

            var sale = filterOrder.Select(o => new _SaleDetail
            {
                UserName = entities.User.FirstOrDefault(u => u.UserID == o.UserID).UserName,
                OrderUserID = o.OrderUserID.Value,
                OrderNumber = o.OrderNumber,
                OrderDate = o.OrderDateTime,
                Barcode = entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).Barcode,
                BranchName = entities.Branch.FirstOrDefault(b => b.BranchID == entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).BranchID).Name,
                Category = entities.Category.FirstOrDefault(c => c.CategoryID == entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).CategoryID.Value).Name,
                Discount = o.Discount,
                ProductID = o.ProductID,
                ProductName = entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).Name,
                ProductPrice = o.Price,
                Quantity = o.AmountF + o.AmountL + o.AmountM + o.AmountS + o.AmountXL + o.AmountXS,
                TotalPrice = o.Price.Value - o.Discount ?? 0
            });

            return View(sale.ToList());
        }

        [Authorize]
        public ActionResult AdminOrderApplyDateRange(string start, string end)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (string.IsNullOrEmpty(start) || string.IsNullOrEmpty(end))
            {
                return RedirectToAction("AdminOrder");
            }

            var startDate = DateTime.Parse(start);
            var endDate = DateTime.Parse(end);
            endDate = endDate.AddHours(23).AddMinutes(59).AddSeconds(59);

            var userId = int.Parse(Session["UserID"].ToString());
            var user = entities.User.FirstOrDefault(u => u.UserID == userId);
            var order = entities.Order.Where(o => o.OrderDateTime >= startDate && o.OrderDateTime <= endDate).GroupBy(o => o.OrderNumber).Select(o => new _OrderStatus
            {
                UserName = entities.User.FirstOrDefault(u => u.UserID == o.FirstOrDefault().UserID).UserName,
                OrderUserID = o.FirstOrDefault().OrderUserID.Value,
                ShopName = entities.User.FirstOrDefault(u => u.UserID == o.FirstOrDefault().UserID).ShopName,
                OrderNumber = o.Key,
                DeliveryAddress = o.FirstOrDefault().DeliveryAddress,
                Remark = o.FirstOrDefault().Remark,
                DeliveryName = o.FirstOrDefault().DeliveryName,
                OrderDate = o.FirstOrDefault().OrderDateTime,
                Status = o.FirstOrDefault().Status
            });

            return View("AdminOrder", order.ToList());
        }

        [Authorize]
        public ActionResult AdminSaleSummaryReport(string start, string end, string filter, string summary)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (string.IsNullOrEmpty(start) || string.IsNullOrEmpty(end))
            {
                return RedirectToAction("AdminSaleSummary");
            }

            var startDate = DateTime.Parse(start);
            var endDate = DateTime.Parse(end);
            endDate = endDate.AddHours(23).AddMinutes(59).AddSeconds(59);

            if (!string.IsNullOrEmpty(filter))
            {
                var sale = entities.Order.Where(o => o.OrderDateTime >= startDate).Where(o => o.OrderDateTime <= endDate).Select(o => new _SaleDetail
                {
                    UserName = entities.User.FirstOrDefault(u => u.UserID == o.UserID).UserName,
                    OrderUserID = o.OrderUserID.Value,
                    OrderNumber = o.OrderNumber,
                    OrderDate = o.OrderDateTime,
                    Barcode = entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).Barcode,
                    BranchName = entities.Branch.FirstOrDefault(b => b.BranchID == entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).BranchID).Name,
                    Category = entities.Category.FirstOrDefault(c => c.CategoryID == entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).CategoryID.Value).Name,
                    Discount = o.Discount,
                    ProductID = o.ProductID,
                    ProductName = entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID.Value).Name,
                    ProductPrice = o.Price,
                    Quantity = o.AmountF + o.AmountL + o.AmountM + o.AmountS + o.AmountXL + o.AmountXS,
                    TotalPrice = o.Price.Value - o.Discount ?? 0
                }).ToList();

                return View("AdminSaleSummary", sale);
            }

            if (!string.IsNullOrEmpty(summary))
            {
                System.Globalization.DateTimeFormatInfo mfi = new
                System.Globalization.DateTimeFormatInfo();

                var listOfMonth = new List<DateTime>();
                var currentBeginningOfMonth = new DateTime(startDate.Year, startDate.Month, 1);

                while (currentBeginningOfMonth <= endDate)
                {
                    listOfMonth.Add(currentBeginningOfMonth);
                    currentBeginningOfMonth = currentBeginningOfMonth.AddMonths(1);
                }

                var summaryList = new List<_SaleSummary>();

                foreach (var branch in entities.Branch.ToList())
                {
                    foreach (var category in entities.Category.ToList())
                    {
                        var filteredOrder = entities.Order.Where(o => entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID).BranchID == branch.BranchID && entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID).CategoryID == category.CategoryID);
                        var orderSumPrice = filteredOrder.Sum(o => o.Price) ?? 0;
                        var orderSumProfit = orderSumPrice - filteredOrder.Sum(o => o.Discount) ?? 0;
                        var orderQuantity = filteredOrder.Sum(o => o.AmountF + o.AmountL + o.AmountM + o.AmountS + o.AmountXL + o.AmountXS);
                        var monthReports = new List<_MonthReport>();

                        foreach (var date in listOfMonth)
                        {
                            var endOfMonth = date.AddMonths(1).AddSeconds(-1);
                            var monthFilterOrder = filteredOrder.Where(o => o.OrderDateTime >= date && o.OrderDateTime <= endOfMonth);
                            var monthPrice = monthFilterOrder.Sum(o => o.Price);
                            var monthProfit = monthPrice - monthFilterOrder.Sum(o => o.Discount);
                            var monthQuantity = monthFilterOrder.Sum(o => o.AmountF + o.AmountL + o.AmountM + o.AmountS + o.AmountXL + o.AmountXS);
                            monthReports.Add(new _MonthReport
                            {
                                MonthName = mfi.GetMonthName(date.Month).ToString() + " " + date.Year,
                                Price = monthProfit ?? 0,
                                Quantity = monthQuantity ?? 0
                            });
                        }

                        summaryList.Add(new _SaleSummary
                        {

                            BranchName = branch.Name,
                            Category = category.Name,
                            MonthReports = monthReports,
                            GrandTotal = new _MonthReport
                            {
                                MonthName = "",
                                Price = orderSumProfit,
                                Quantity = orderQuantity ?? 0
                            }
                        });
                    }

                }

                return View(summaryList);
            }

            return RedirectToAction("AdminSaleSummary");
        }

        [Authorize]
        public ActionResult AdminOrderProduct()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }
            var userList = new[] { new { userID = 0, Name = "*** กรุณาเลือกตัวแทนที่่จะสั่งของให้ ***" } }.ToList();
            userList.AddRange(entities.User.Where(u => u.UserType != 1 && u.UserType != 9).OrderBy(u => u.UserName).Select(u => new { userID = u.UserID, Name = u.UserName + " (" + u.ShopName + ") [" + u.MemberCode + "]" }).ToList());

            ViewBag.userList = userList;
            var product = entities.Product.Where(p => p.BranchID == 1).OrderBy(p => p.SortID).ToList();

            return View(product);
        }

        [Authorize]
        [HttpPost]
        public string AdminOrderProductDummy(string id, string value)
        {
            return value;
        }

        [HttpPost]
        [Authorize]
        public JsonResult SetImpersonateUser(string id)
        {
            var userID = int.Parse(id);
            var user = entities.User.FirstOrDefault(u => u.UserID == userID);
            Session["impersonateUserID"] = id;
            Session["impersonateUserType"] = user.UserType;
            return Json(user);
        }

        [Authorize]
        public ActionResult AdminCategory()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var categories = entities.Category.ToList();

            return View(categories);
        }

        [Authorize]
        [HttpGet]
        public ActionResult AdminOrderUpdateStatus(string orderNumber, int status, string remark = "")
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            string orderNo;

            var tokenId = orderNumber.Split('_');
            if (tokenId.Length > 1)
            {
                orderNo = tokenId[1];
            }
            else
            {
                orderNo = tokenId[0];
            }

            var orders = entities.Order.Where(o => o.OrderNumber == orderNo);

            foreach (var order in orders)
            {
                order.Status = status;
                if (!string.IsNullOrEmpty(remark))
                {
                    if (status == 3)
                    {
                        order.Shipping = Convert.ToInt32(remark);
                    }
                    else
                    {
                        order.Remark = remark;
                    }

                }
            }

            entities.SaveChanges();

            return RedirectToAction("AdminOrder");
        }

        [Authorize]
        public ActionResult AdminProduct()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var userID = int.Parse(Session["UserID"].ToString());
            var userType = int.Parse(Session["UserType"].ToString());

            var categoryList = new List<Category> { new Category { CategoryID = 0, Name = "*กรุณาเลือกหมวดหมู่*", Description = "", L = false, M = false, S = false, XL = false, XS = false, XXL = false } };

            categoryList.AddRange(entities.Category.ToList());

            ViewBag.categoryList = categoryList;

            ViewBag.branchList = entities.Branch.ToList();

            var user = entities.User.FirstOrDefault(u => u.UserID == userID);
            var product = new List<_ProductView>();
            if (userType == 9)
            {
                product = entities.Product.Where(p => p.BranchID == user.BranchID).OrderBy(p => p.SortID).Select(p => new _ProductView
                {
                    AmountF = p.AmountF,
                    AmountL = p.AmountL,
                    AmountM = p.AmountM,
                    AmountS = p.AmountS,
                    AmountXL = p.AmountXL,
                    AmountXS = p.AmountXS,
                    Barcode = p.Barcode,
                    BranchID = p.BranchID,
                    BranchName = entities.Branch.FirstOrDefault(b => b.BranchID == p.BranchID).Name,
                    Name = p.Name,
                    CategoryID = p.CategoryID,
                    CategoryName = entities.Category.FirstOrDefault(c => c.CategoryID == p.CategoryID).Name,
                    Image = p.Image,
                    ImageContentType = p.ImageContentType,
                    ImageFileName = p.ImageFileName,
                    Price = p.Price,
                    Price2 = p.Price2,
                    Price3 = p.Price3,
                    Price4 = p.Price4,
                    PriceVIP = p.PriceVIP,
                    ProductID = p.ProductID,
                    SortID = p.SortID
                }).ToList();
            }
            else
            {
                product = entities.Product.OrderBy(p => p.SortID).Select(p => new _ProductView
                {
                    AmountF = p.AmountF,
                    AmountL = p.AmountL,
                    AmountM = p.AmountM,
                    AmountS = p.AmountS,
                    AmountXL = p.AmountXL,
                    AmountXS = p.AmountXS,
                    Barcode = p.Barcode,
                    BranchID = p.BranchID,
                    BranchName = entities.Branch.FirstOrDefault(b => b.BranchID == p.BranchID).Name,
                    Name = p.Name,
                    CategoryID = p.CategoryID,
                    CategoryName = entities.Category.FirstOrDefault(c => c.CategoryID == p.CategoryID).Name,
                    Image = p.Image,
                    ImageContentType = p.ImageContentType,
                    ImageFileName = p.ImageFileName,
                    Price = p.Price,
                    Price2 = p.Price2,
                    Price3 = p.Price3,
                    Price4 = p.Price4,
                    PriceVIP = p.PriceVIP,
                    ProductID = p.ProductID,
                    SortID = p.SortID
                }).ToList();
            }
            return View(product);
        }

        [Authorize]
        public ActionResult ImageInserter(string id, HttpPostedFileBase Image)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            //FileInfo excel = new FileInfo(@"C:\Users\cchangboonchu\Downloads\SALE Detail.xlsx");
            //using (var package = new ExcelPackage(excel))
            //{
            //    var workbook = package.Workbook;
            ////*** Sheet 1
            //var worksheet = workbook.Worksheets.First();

            //entities.Order.RemoveRange(entities.Order.ToList());

            //entities.SaveChanges();

            //var persistSaleID = "";
            //var persistInvoiceID = "";
            //var persistDate = new DateTime();
            //var persistBranchID = "";
            //var persistBatchRef = Guid.NewGuid();
            ////var orders = entities.Order.ToList();
            //var products = entities.Product.ToList();
            //var OrderList = new List<Order>();

            //    for (var i = 2; i <= worksheet.Dimension.End.Row; i++)
            //    {
            //        var saleID = worksheet.Cells[i, 1].Value ?? "";
            //        var InvoiceID = worksheet.Cells[i, 2].Value ?? "";
            //        var Date = worksheet.Cells[i, 3].Value ?? "";
            //        var BranchID = worksheet.Cells[i, 4].Value ?? "";
            //        var CategoryName = worksheet.Cells[i, 5].Value ?? "";
            //        var Barcode = worksheet.Cells[i, 6].Value ?? "";
            //        var ProductCode = worksheet.Cells[i, 7].Value ?? "";
            //        var ProductName = worksheet.Cells[i, 8].Value ?? "";
            //        var Price = worksheet.Cells[i, 9].Value ?? "";
            //        var Amount = worksheet.Cells[i, 10].Value ?? "";
            //        var Discount = worksheet.Cells[i, 11].Value ?? "";
            //        var SalePrice = worksheet.Cells[i, 12].Value ?? "";
            //        var Tax = worksheet.Cells[i, 13].Value ?? "";
            //        var Cost = worksheet.Cells[i, 14].Value ?? "";
            //        var LossProfit = worksheet.Cells[i, 15].Value ?? "";
            //        var Month = worksheet.Cells[i, 16].Value ?? "";
            //        if (saleID != null && !string.IsNullOrEmpty(saleID.ToString()))
            //        {
            //            persistSaleID = saleID.ToString();
            //            persistBatchRef = Guid.NewGuid();
            //        }
            //        if (InvoiceID != null) persistInvoiceID = InvoiceID.ToString();
            //        if (Date != null && !string.IsNullOrEmpty(Date.ToString())) persistDate = Convert.ToDateTime(Date);
            //        if (BranchID != null) persistBranchID = BranchID.ToString();

            //        var thisBarcode = "";
            //        var thisSize = "";
            //        var amountXS = 0;
            //        var amountS = 0;
            //        var amountM = 0;
            //        var amountL = 0;
            //        var amountXL = 0;
            //        var amountXXL = 0;
            //        var actualDiscount = Discount == "" ? 0 : int.Parse(Discount.ToString());
            //        var actualSalePrice = SalePrice == "" ? 0 : int.Parse(SalePrice.ToString());

            //        if (Barcode.ToString().EndsWith("XXL"))
            //        {
            //            thisBarcode = Barcode.ToString().Substring(0, Barcode.ToString().Length - 3);
            //            thisSize = "XXL";
            //            amountXXL = string.IsNullOrEmpty(Amount.ToString()) ? 0 : Convert.ToInt32(Amount);
            //        }
            //        else if (Barcode.ToString().EndsWith("XL"))
            //        {
            //            thisBarcode = Barcode.ToString().Substring(0, Barcode.ToString().Length - 2);
            //            thisSize = "XL";
            //            amountXL = string.IsNullOrEmpty(Amount.ToString()) ? 0 : Convert.ToInt32(Amount);
            //        }
            //        else if (Barcode.ToString().EndsWith("XS"))
            //        {
            //            thisBarcode = Barcode.ToString().Substring(0, Barcode.ToString().Length - 2);
            //            thisSize = "XS";
            //            amountXS = string.IsNullOrEmpty(Amount.ToString()) ? 0 : Convert.ToInt32(Amount);
            //        }
            //        else if (Barcode.ToString().EndsWith("L"))
            //        {
            //            thisBarcode = Barcode.ToString().Substring(0, Barcode.ToString().Length - 1);
            //            thisSize = "L";
            //            amountL = string.IsNullOrEmpty(Amount.ToString()) ? 0 : Convert.ToInt32(Amount);
            //        }
            //        else if (Barcode.ToString().EndsWith("M"))
            //        {
            //            thisBarcode = Barcode.ToString().Substring(0, Barcode.ToString().Length - 1);
            //            thisSize = "M";
            //            amountM = string.IsNullOrEmpty(Amount.ToString()) ? 0 : Convert.ToInt32(Amount);
            //        }
            //        else if (Barcode.ToString().EndsWith("S"))
            //        {
            //            thisBarcode = Barcode.ToString().Substring(0, Barcode.ToString().Length - 1);
            //            thisSize = "S";
            //            amountS = string.IsNullOrEmpty(Amount.ToString()) ? 0 : Convert.ToInt32(Amount);
            //        }

            //        var product =
            //            products.FirstOrDefault(p => p.Barcode.ToUpper() == thisBarcode.Trim().ToUpper().Replace("'", ""));

            //        if (product == null) continue;

            //        var order = OrderList.FirstOrDefault(o => o.BatchRef == persistBatchRef && o.ProductID == product.ProductID);

            //        if (order == null)
            //        {
            //            order = new Order
            //            {
            //                BatchRef = persistBatchRef,
            //                ProductID = product.ProductID,
            //                AmountF = amountXXL,
            //                AmountL = amountL,
            //                AmountM = amountM,
            //                AmountS = amountS,
            //                AmountXL = amountXL,
            //                AmountXS = amountXS,
            //                OrderDateTime = persistDate,
            //                Discount = actualDiscount,
            //                OrderNumber = persistSaleID,
            //                OrderUserID = 1,
            //                Price = actualSalePrice
            //            };
            //            OrderList.Add(order);
            //        }

            //        else
            //        {
            //            order.ProductID = product.ProductID;
            //            order.AmountF = amountXXL == 0 ? product.AmountF : 0;
            //            order.AmountL = amountL == 0 ? product.AmountL : 0;
            //            order.AmountM = amountM == 0 ? product.AmountM : 0;
            //            order.AmountS = amountS == 0 ? product.AmountS : 0;
            //            order.AmountXL = amountXL == 0 ? product.AmountXL : 0;
            //            order.AmountXS = amountXS == 0 ? product.AmountXS : 0;
            //            order.Price = actualSalePrice;
            //            order.Discount = actualDiscount;
            //        }
            //    }

            //    entities.Order.AddRange(OrderList);
            //    entities.SaveChanges();
            //}


            FileInfo excel2 = new FileInfo(@"C:\Users\cchangboonchu\Downloads\7stock.xlsx");
            using (var package = new ExcelPackage(excel2))
            {
                var workbook = package.Workbook;

                SecureString secret = ConvertToSecureString("test");

                var test = secret.ToString();

                //*** Sheet 1
                var worksheet = workbook.Worksheets.First();
                var productBarcode = "";

                for (var i = 2; i <= worksheet.Dimension.End.Row; i++)
                {
                    var barcodeStr = worksheet.Cells[i, 1].Value.ToString();
                    var stockStr = worksheet.Cells[i, 6].Value.ToString();

                    if (barcodeStr.EndsWith("XXL"))
                    {
                        productBarcode = barcodeStr.Substring(0, barcodeStr.Length - 3);
                    }
                    else if (barcodeStr.EndsWith("XL"))
                    {
                        productBarcode = barcodeStr.Substring(0, barcodeStr.Length - 2);
                    }
                    else if (barcodeStr.EndsWith("XS"))
                    {
                        productBarcode = barcodeStr.Substring(0, barcodeStr.Length - 2);
                    }
                    else if (barcodeStr.EndsWith("L"))
                    {
                        productBarcode = barcodeStr.Substring(0, barcodeStr.Length - 1);
                    }
                    else if (barcodeStr.EndsWith("M"))
                    {
                        productBarcode = barcodeStr.Substring(0, barcodeStr.Length - 1);
                    }
                    else if (barcodeStr.EndsWith("S"))
                    {
                        productBarcode = barcodeStr.Substring(0, barcodeStr.Length - 1);
                    }

                    var product = entities.Product.FirstOrDefault(p => p.Barcode == productBarcode);

                    if (product != null)
                    {
                        if (barcodeStr.EndsWith("XXL"))
                        {
                            product.AmountF = int.Parse(stockStr);
                        }
                        else if (barcodeStr.EndsWith("XL"))
                        {
                            product.AmountXL = int.Parse(stockStr);
                        }
                        else if (barcodeStr.EndsWith("XS"))
                        {
                            product.AmountXS = int.Parse(stockStr);
                        }
                        else if (barcodeStr.EndsWith("L"))
                        {
                            product.AmountL = int.Parse(stockStr);
                        }
                        else if (barcodeStr.EndsWith("M"))
                        {
                            product.AmountM = int.Parse(stockStr);
                        }
                        else if (barcodeStr.EndsWith("S"))
                        {
                            product.AmountS = int.Parse(stockStr);
                        }

                        entities.SaveChanges();
                    }

                }
            }

            if (string.IsNullOrEmpty(id))
            {
                return View(new Product());
            }

            return View(entities.Product.FirstOrDefault(p => p.Barcode.ToUpper() == id.ToUpper()));
        }

        private SecureString ConvertToSecureString(string str)
        {
            var secureStr = new SecureString();
            if (str.Length > 0)
            {
                foreach (var c in str.ToCharArray()) secureStr.AppendChar(c);
            }
            return secureStr;
        }

        private DataTable ConvertToDataTable(ExcelWorksheet oSheet)
        {
            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            for (int i = 1; i <= totalRows; i++)
            {
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                    else
                        dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                }
            }
            return dt;
        }

        [Authorize]
        [HttpPost]
        public ActionResult UploadImage(string Barcode, IEnumerable<HttpPostedFileBase> Images)
        {
            foreach (var image in Images)
            {
                if (image != null)
                {
                    var filename = image.FileName.Split('.')[0];
                    var products = entities.Product.Where(p => p.Barcode == filename);

                    if (products != null)
                    {
                        using (var reader = new BinaryReader(image.InputStream))
                        {
                            var imageContent = reader.ReadBytes(image.ContentLength);

                            foreach (var product in products)
                            {
                                product.Image = imageContent;
                                product.ImageContentType = image.ContentType;
                                product.ImageFileName = image.FileName;
                            }
                        }
                    }

                    entities.SaveChanges();
                }
            }


            return RedirectToAction("ImageInserter", new { id = Barcode });
        }

        [Authorize]
        public ActionResult AdminTransferProduct()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var userId = int.Parse(Session["UserID"].ToString());

            var user = entities.User.FirstOrDefault(u => u.UserID == userId);

            Session["userBranchID"] = user.BranchID;

            ViewBag.branchlist = entities.Branch.ToList();

            var products = entities.Product.Where(p => p.BranchID == 1).ToList();

            var transactionList = entities.Transaction.Select(t => new _TransactionDetail
            {
                AmountL = t.AmountL,
                AmountM = t.AmountM,
                AmountS = t.AmountS,
                AmountXL = t.AmountXL,
                AmountXS = t.AmountXS,
                AmountXXL = t.AmountXXL,
                CategoryName = entities.Category.FirstOrDefault(c => c.CategoryID == entities.Product.FirstOrDefault(p => p.ProductID == t.ProductID).CategoryID.Value).Name,
                Date = t.Date,
                FromBranch = entities.Branch.FirstOrDefault(b => b.BranchID == t.FromBranchID).Name,
                ProductName = entities.Product.FirstOrDefault(p => p.ProductID == t.ProductID).Name,
                ToBranch = entities.Branch.FirstOrDefault(b => b.BranchID == t.ToBranchID).Name,
                TransactionNumber = t.TransactionNumber,
                TransactionID = t.TransactionID,
                UserName = entities.User.FirstOrDefault(u => u.UserID == t.UserID).UserName,
                FromBranchID = t.FromBranchID,
                UserID = t.UserID,
                ProductID = t.ProductID,
                SupplierID = t.SupplierID,
                ToBranchID = t.ToBranchID,
                Approved = t.Approved
            }).ToList();

            if (user.UserType == 9)
            {
                transactionList = transactionList.Where(t => t.ToBranchID.Value == user.BranchID.Value || t.FromBranchID == user.BranchID.Value).ToList();
                ViewBag.branchlist = entities.Branch.Where(b => b.BranchID == user.BranchID).ToList();
            }

            return View(new _TransactionList
            {
                products = products,
                transactions = transactionList
            });
        }

        [Authorize]
        public ActionResult AdminTransferProductTo()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (Session["OriginBranchID"] == null)
            {
                return RedirectToAction("AdminTransferProduct");
            }

            var userId = int.Parse(Session["UserID"].ToString());

            var user = entities.User.FirstOrDefault(u => u.UserID == userId);

            var branchID = int.Parse(Session["OriginBranchID"].ToString());

            ViewBag.branchlist = entities.Branch.ToList();

            var products = entities.Product.Where(p => p.BranchID == branchID).ToList();

            var transactionList = entities.Transaction.Select(t => new _TransactionDetail
            {
                AmountL = t.AmountL,
                AmountM = t.AmountM,
                AmountS = t.AmountS,
                AmountXL = t.AmountXL,
                AmountXS = t.AmountXS,
                AmountXXL = t.AmountXXL,
                CategoryName = entities.Category.FirstOrDefault(c => c.CategoryID == entities.Product.FirstOrDefault(p => p.ProductID == t.ProductID).CategoryID.Value).Name,
                Date = t.Date,
                FromBranch = entities.Branch.FirstOrDefault(b => b.BranchID == t.FromBranchID).Name,
                ProductName = entities.Product.FirstOrDefault(p => p.ProductID == t.ProductID).Name,
                ToBranch = entities.Branch.FirstOrDefault(b => b.BranchID == t.ToBranchID).Name,
                TransactionNumber = t.TransactionNumber,
                TransactionID = t.TransactionID,
                UserName = entities.User.FirstOrDefault(u => u.UserID == t.UserID).UserName,
                FromBranchID = t.FromBranchID,
                UserID = t.UserID,
                ProductID = t.ProductID,
                SupplierID = t.SupplierID,
                ToBranchID = t.ToBranchID
            }).ToList();

            if (user.UserType == 9)
            {
                transactionList = transactionList.Where(t => t.ToBranchID.Value == user.BranchID.Value).ToList();
            }

            return View(new _TransactionList
            {
                products = products,
                transactions = transactionList
            });
        }

        [HttpPost]
        [Authorize]
        public string AdminTransferProductTransaction(string id, string value)
        {
            try
            {
                var amountOrder = int.Parse(value);
                int amountAvail = 0;

                var tokenId = id.Split('_');

                var ProductId = int.Parse(tokenId[2]);

                var product = entities.Product.FirstOrDefault(u => u.ProductID == ProductId);

                List<Transaction> transactions;

                if (Session["TransferProductTransaction"] == null)
                {
                    transactions = new List<Transaction>();
                }
                else
                {
                    transactions = Session["TransferProductTransaction"] as List<Transaction>;
                }

                var NewTransaction = new Transaction();

                if (product != null)
                {
                    if (transactions.Any(t => t.ProductID == product.ProductID))
                    {
                        NewTransaction = transactions.FirstOrDefault(t => t.ProductID == product.ProductID);
                        transactions.Remove(transactions.FirstOrDefault(t => t.ProductID == product.ProductID));
                    }
                    else
                    {
                        NewTransaction.ProductID = product.ProductID;
                    }

                    switch (tokenId[1])
                    {
                        case "amountxs":
                            amountAvail = product.AmountXS ?? 0;
                            amountOrder = amountOrder > amountAvail ? amountAvail : amountOrder;
                            NewTransaction.AmountXS = NewTransaction.AmountXS = amountOrder;

                            break;
                        case "amounts":
                            amountAvail = product.AmountS ?? 0;
                            amountOrder = amountOrder > amountAvail ? amountAvail : amountOrder;
                            NewTransaction.AmountS = NewTransaction.AmountS = amountOrder;

                            break;
                        case "amountm":
                            amountAvail = product.AmountM ?? 0;
                            amountOrder = amountOrder > amountAvail ? amountAvail : amountOrder;
                            NewTransaction.AmountM = NewTransaction.AmountM = amountOrder;

                            break;
                        case "amountl":
                            amountAvail = product.AmountL ?? 0;
                            amountOrder = amountOrder > amountAvail ? amountAvail : amountOrder;
                            NewTransaction.AmountL = NewTransaction.AmountL = amountOrder;

                            break;
                        case "amountxl":
                            amountAvail = product.AmountXL ?? 0;
                            amountOrder = amountOrder > amountAvail ? amountAvail : amountOrder;
                            NewTransaction.AmountXL = NewTransaction.AmountXL = amountOrder;

                            break;
                        case "amountf":
                            amountAvail = product.AmountF ?? 0;
                            amountOrder = amountOrder > amountAvail ? amountAvail : amountOrder;
                            NewTransaction.AmountXXL = NewTransaction.AmountXXL = amountOrder;

                            break;
                    }

                    transactions.Add(NewTransaction);

                    Session["TransferProductTransaction"] = transactions;
                }

                return amountOrder.ToString();
            }
            catch (Exception)
            {
                return "0";
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult AdminTransferTransactions(string ToBranchDDL)
        {
            try
            {
                var userID = Session["UserID"] == null ? 0 : int.Parse(Session["UserID"].ToString());
                var toBranchID = int.Parse(ToBranchDDL);
                var fromBranchID = Session["OriginBranchID"] == null ? 0 : int.Parse(Session["OriginBranchID"].ToString());
                var createDate = DateTime.Now;
                var transactionNumber = string.IsNullOrEmpty(entities.Transaction.Max(t => t.TransactionNumber)) ? 1 : int.Parse(entities.Transaction.Max(t => t.TransactionNumber)) + 1;

                var transactions = Session["TransferProductTransaction"] as List<Transaction>;

                foreach (var tran in transactions)
                {
                    tran.UserID = userID;
                    tran.FromBranchID = fromBranchID;
                    tran.ToBranchID = toBranchID;
                    tran.Date = createDate;
                    tran.TransactionNumber = transactionNumber.ToString();

                }

                entities.Transaction.AddRange(transactions);
                entities.SaveChanges();

                //foreach (var tran in transactions)
                //{
                //    //temp add stock, later will need approval
                //    var prod = entities.Product.FirstOrDefault(p => p.ProductID == tran.ProductID && p.BranchID == tran.ToBranchID);
                //    prod.AmountF += tran.AmountXXL ?? 0;
                //    prod.AmountL += tran.AmountL ?? 0;
                //    prod.AmountM += tran.AmountM ?? 0;
                //    prod.AmountS += tran.AmountS ?? 0;
                //    prod.AmountXL += tran.AmountXL ?? 0;
                //    prod.AmountXS += tran.AmountXS ?? 0;
                //}
                //entities.SaveChanges();

                Session["TransferProductTransaction"] = null;
                Session["OriginBranchID"] = null;

                return RedirectToAction("AdminTransferProduct");
            }
            catch (Exception)
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult ApproveProductTransfer(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var transactionId = int.Parse(tokenId[1]);
                var transaction = entities.Transaction.FirstOrDefault(t => t.TransactionID == transactionId);
                if (transaction != null)
                {
                    var transactionNumber = transaction.TransactionNumber;

                    var transactionList = entities.Transaction.Where(t => t.TransactionNumber == transactionNumber).ToList();

                    foreach (var tran in transactionList)
                    {
                        var productFrom = entities.Product.FirstOrDefault(p => p.ProductID == tran.ProductID);

                        var productTo = entities.Product.FirstOrDefault(p => p.Barcode == productFrom.Barcode && p.BranchID == tran.ToBranchID);

                        productFrom.AmountXS -= tran.AmountXS;
                        productFrom.AmountS -= tran.AmountS;
                        productFrom.AmountM -= tran.AmountM;
                        productFrom.AmountL -= tran.AmountL;
                        productFrom.AmountXL -= tran.AmountXL;
                        productFrom.AmountF -= tran.AmountXXL;

                        productTo.AmountXS += tran.AmountXS;
                        productTo.AmountS += tran.AmountS;
                        productTo.AmountM += tran.AmountM;
                        productTo.AmountL += tran.AmountL;
                        productTo.AmountXL += tran.AmountXL;
                        productTo.AmountF += tran.AmountXXL;

                        tran.Approved = true;
                    }

                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminTransferProduct", "Main"));
        }

        [HttpPost]
        [Authorize]
        public ActionResult CancelProductTransfer(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var transactionId = int.Parse(tokenId[1]);
                var transaction = entities.Transaction.FirstOrDefault(t => t.TransactionID == transactionId);
                if (transaction != null)
                {
                    var transactionNumber = transaction.TransactionNumber;

                    var transactionList = entities.Transaction.Where(t => t.TransactionNumber == transactionNumber).ToList();

                    entities.Transaction.RemoveRange(transactionList);
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminTransferProduct", "Main"));
        }

        [Authorize]
        public ActionResult AdminSupplier()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var suppliers = entities.Supplier.ToList();

            return View(suppliers);
        }

        [HttpPost]
        [Authorize]
        public JsonResult AddScannedProduct(string code)
        {

            var barcode = "";
            var size = "";
            var elementId = "td_amount";

            if (code.ToUpper().EndsWith("XXL"))
            {
                barcode = code.Substring(0, code.Length - 3);
                size = "f";
            }
            else if (code.ToUpper().EndsWith("XL"))
            {
                barcode = code.Substring(0, code.Length - 2);
                size = "xl";
            }
            else if (code.ToUpper().EndsWith("XS"))
            {
                barcode = code.Substring(0, code.Length - 2);
                size = "xs";
            }
            else if (code.ToUpper().EndsWith("S"))
            {
                barcode = code.Substring(0, code.Length - 1);
                size = "s";
            }
            else if (code.ToUpper().EndsWith("M"))
            {
                barcode = code.Substring(0, code.Length - 1);
                size = "m";
            }
            else if (code.ToUpper().EndsWith("L"))
            {
                barcode = code.Substring(0, code.Length - 1);
                size = "l";
            }
            else if (code.ToUpper().EndsWith("F"))
            {
                barcode = code.Substring(0, code.Length - 1);
                size = "f";
            }
            else if (code == "4511413405208") //For testing only
            {
                barcode = code;
                size = "f";
            }

            var product = entities.Product.FirstOrDefault(p => p.Barcode.ToUpper() == barcode.ToUpper());

            if (product != null)
            {
                elementId += size + "_" + product.ProductID;
            }

            return Json(new { id = elementId });
        }

        [Authorize]
        public ActionResult AdminAddProduct()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var categoryList = new List<Category> { new Category { CategoryID = 0, Name = "*กรุณาเลือกหมวดหมู่*", Description = "", L = false, M = false, S = false, XL = false, XS = false, XXL = false } };

            categoryList.AddRange(entities.Category.ToList());

            ViewBag.categoryList = categoryList;
            ViewBag.branchList = entities.Branch.ToList();
            ViewBag.supplierList = entities.Supplier.ToList();
            Session["SupplierID"] = entities.Supplier.FirstOrDefault().SupplierID;
            var product = entities.Product.Where(p => p.BranchID == 1).OrderBy(p => p.SortID).ToList();
            return View(product);
        }

        [Authorize]
        [HttpPost]
        public void SetSupplier(string id)
        {
            Session["SupplierID"] = int.Parse(id);
        }

        [Authorize]
        [HttpPost]
        public ActionResult SetOriginTransfer(string FromBranch)
        {
            Session["OriginBranchID"] = int.Parse(FromBranch);

            return RedirectToAction("AdminTransferProductTo");
        }

        [Authorize]
        [HttpPost]
        public string AdminProductTableUpdate(string id, string value)
        {
            var tokenId = id.Split('_');

            var ProductId = int.Parse(tokenId[2]);

            var product = entities.Product.FirstOrDefault(u => u.ProductID == ProductId);

            if (product != null)
            {
                switch (tokenId[1])
                {
                    case "name":
                        product.Barcode = value;
                        break;
                    case "description":
                        product.Name = value;
                        break;
                    case "image":
                        //product.Image = value;
                        break;
                    case "amountxs":
                        product.AmountXS = int.Parse(value);
                        break;
                    case "amounts":
                        product.AmountS = int.Parse(value);
                        break;
                    case "amountm":
                        product.AmountM = int.Parse(value);
                        break;
                    case "amountl":
                        product.AmountL = int.Parse(value);
                        break;
                    case "amountxl":
                        product.AmountXL = int.Parse(value);
                        break;
                    case "amountf":
                        product.AmountF = int.Parse(value);
                        break;
                    case "categoryid":
                        product.CategoryID = int.Parse(value);
                        break;
                    case "barcode":
                        product.Barcode = value;
                        break;
                    case "price":
                        product.Price = int.Parse(value);
                        break;
                    case "price2":
                        product.Price2 = int.Parse(value);
                        break;
                    case "price3":
                        product.Price3 = int.Parse(value);
                        break;
                    case "pricevip":
                        product.PriceVIP = int.Parse(value);
                        break;
                    case "branch":
                        product.BranchID = int.Parse(value);
                        break;
                    case "sortid":
                        int sortID;
                        if (int.TryParse(value, out sortID))
                        {
                            if (sortID > 0 && sortID < 1000)
                            {
                                product.SortID = sortID;
                            }
                            else return product.SortID.Value.ToString("D3");
                        }
                        else return product.SortID.Value.ToString("D3");
                        break;
                }

                entities.SaveChanges();

                return value;
            }

            return value;
        }

        [Authorize]
        [HttpPost]
        public string AdminAddProductTransaction(string id, string value)
        {
            try
            {
                var tokenId = id.Split('_');

                var ProductId = int.Parse(tokenId[2]);

                var product = entities.Product.FirstOrDefault(u => u.ProductID == ProductId);

                List<Transaction> transactions;

                if (Session["AddProductTransaction"] == null)
                {
                    transactions = new List<Transaction>();
                }
                else
                {
                    transactions = Session["AddProductTransaction"] as List<Transaction>;
                }

                var NewTransaction = new Transaction();

                if (product != null)
                {
                    if (transactions.Any(t => t.ProductID == product.ProductID))
                    {
                        NewTransaction = transactions.FirstOrDefault(t => t.ProductID == product.ProductID);
                        transactions.Remove(transactions.FirstOrDefault(t => t.ProductID == product.ProductID));
                    }
                    else
                    {
                        NewTransaction.ProductID = product.ProductID;
                    }

                    switch (tokenId[1])
                    {
                        case "amountxs":
                            NewTransaction.AmountXS = NewTransaction.AmountXS = int.Parse(value);
                            break;
                        case "amounts":
                            NewTransaction.AmountS = NewTransaction.AmountS = int.Parse(value);
                            break;
                        case "amountm":
                            NewTransaction.AmountM = NewTransaction.AmountM = int.Parse(value);
                            break;
                        case "amountl":
                            NewTransaction.AmountL = NewTransaction.AmountL = int.Parse(value);
                            break;
                        case "amountxl":
                            NewTransaction.AmountXL = NewTransaction.AmountXL = int.Parse(value);
                            break;
                        case "amountf":
                            NewTransaction.AmountXXL = NewTransaction.AmountXXL = int.Parse(value);
                            break;
                    }

                    transactions.Add(NewTransaction);

                    Session["AddProductTransaction"] = transactions;
                    return value;
                }
                return value;
            }
            catch (Exception)
            {
                return "0";
            }
        }

        [Authorize]
        [HttpPost]
        public string AdminAddTransactions()
        {
            try
            {
                var userID = Session["UserID"] == null ? 0 : int.Parse(Session["UserID"].ToString());
                var supplierID = Session["SupplierID"] == null ? 0 : int.Parse(Session["SupplierID"].ToString());
                var toBranchID = 1;
                var createDate = DateTime.Now;
                var transactionNumber = string.IsNullOrEmpty(entities.Transaction.Max(t => t.TransactionNumber)) ? 1 : int.Parse(entities.Transaction.Max(t => t.TransactionNumber)) + 1;

                var transactions = Session["AddProductTransaction"] as List<Transaction>;

                foreach (var tran in transactions)
                {
                    tran.UserID = userID;
                    tran.SupplierID = supplierID;
                    tran.ToBranchID = toBranchID;
                    tran.Date = createDate;
                    tran.TransactionNumber = transactionNumber.ToString();

                }

                entities.Transaction.AddRange(transactions);
                entities.SaveChanges();

                foreach (var tran in transactions)
                {
                    //temp add stock, later will need approval
                    var prod = entities.Product.FirstOrDefault(p => p.ProductID == tran.ProductID && p.BranchID == tran.ToBranchID);
                    prod.AmountF += tran.AmountXXL ?? 0;
                    prod.AmountL += tran.AmountL ?? 0;
                    prod.AmountM += tran.AmountM ?? 0;
                    prod.AmountS += tran.AmountS ?? 0;
                    prod.AmountXL += tran.AmountXL ?? 0;
                    prod.AmountXS += tran.AmountXS ?? 0;
                }
                entities.SaveChanges();

                Session["AddProductTransaction"] = null;

                return "success";
            }
            catch (Exception)
            {
                return "failed";
            }
        }

        [Authorize]
        public ActionResult AdminUser()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            ViewBag.ProvinceList = GetProvincesForDDL();
            var adminTypeList = new List<SelectListItem>();
            adminTypeList.Add(new SelectListItem { Text = "Super Admin", Value = "1" });
            adminTypeList.Add(new SelectListItem { Text = "Sub Admin", Value = "9" });
            adminTypeList.Add(new SelectListItem { Text = "Retail User", Value = "5" });
            adminTypeList.Add(new SelectListItem { Text = "VIP User", Value = "6" });
            ViewBag.AdminTypeList = adminTypeList;
            var cityListStr = new List<string>();
            cityListStr.Add("*กรุณาเลือกจังหวัดก่อน*");
            var citySelList = new SelectList(cityListStr);
            ViewBag.CityList = citySelList;  //entities.City.Where(c => c.ProvinceID == 1).Select(c => new SelectListItem { Text = c.Name, Value = c.CityID.ToString() });
            var user = entities.User.ToList();
            return View(user);
        }

        [Authorize]
        public ActionResult AdminUserOrder()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            ViewBag.ProvinceList = GetProvincesForDDL();
            var user = entities.User.Where(u => u.Approved != null && u.UserType != 1).ToList();
            return View(user);
        }

        [Authorize]
        public ActionResult AdminEditOrder(string orderNumber)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var userId = int.Parse(Session["UserID"].ToString());
            var userType = int.Parse(Session["UserType"].ToString());

            var products = new List<Product>();
            var orderList = entities.Order.Where(o => o.OrderNumber == orderNumber).ToList();

            foreach (var orderItem in orderList)
            {
                var productItem = entities.Product.FirstOrDefault(p => p.ProductID == orderItem.ProductID.Value);
                products.Add(new Product
                {
                    AmountF = orderItem.AmountF,
                    AmountL = orderItem.AmountL,
                    AmountM = orderItem.AmountM,
                    AmountS = orderItem.AmountS,
                    AmountXL = orderItem.AmountXL,
                    AmountXS = orderItem.AmountXS,
                    Price = orderItem.Price,
                    ProductID = orderItem.ProductID.Value,
                    Name = productItem.Name,
                    Barcode = productItem.Barcode
                });
            }

            //var productOrders = products.Join(orders, p => p.ProductID, o => o.ProductID, (p, o) => new Product
            //{
            //    AmountXS = o.AmountXS,
            //    AmountS = o.AmountS,
            //    AmountM = o.AmountM,
            //    AmountL = o.AmountL,
            //    AmountXL = o.AmountXL,
            //    AmountF = o.AmountF,
            //    Barcode = p.Barcode,
            //    BranchID = p.BranchID,
            //    CategoryID = p.CategoryID,
            //    Name = p.Name,
            //    Price = p.Price,
            //    Price2 = p.Price2,
            //    Price3 = p.Price3,
            //    PriceVIP = p.PriceVIP,
            //    ProductID = p.ProductID
            //}).GroupBy(o => o.ProductID).Select(group => group.First()).ToList();
            var order = entities.Order.FirstOrDefault(o => o.OrderNumber == orderNumber);

            if (!(userType == 1 || userType == 9))
            {
                return RedirectToAction("Login");
            }

            if (order != null)
            {
                var orderUserID = order.UserID;
                var orderUser = entities.User.FirstOrDefault(u => u.UserID == orderUserID);
                var orderUserType = orderUser.UserType.Value;

                var orderDetails = new _OrderDetail
                {
                    OrderID = order.OrderID,
                    ProductID = order.ProductID,
                    OrderNumber = order.OrderNumber,
                    DeliverDateTime = order.DeliverDateTime,
                    DeliveryAddress = order.DeliveryAddress,
                    DeliveryName = order.DeliveryName,
                    EMSNumber = order.EMSNumber,
                    OrderDateTime = order.OrderDateTime,
                    ProcessDateTime = order.ProcessDateTime,
                    Remark = order.Remark,
                    Status = order.Status,
                    UserID = order.UserID,
                    Products = products,
                    OrderUserID = orderUserID,
                    UserType = orderUserType,
                    Shipping = order.Shipping,
                    Discount = order.Discount
                };

                return View(orderDetails);
            }

            return View();

        }

        private List<Province> GetProvincesForDDL()
        {
            var provinceList = new List<Province> { new Province { Code = "XXX", Name = "*กรุณาเลือกจังหวัด*", ProvinceID = 0 } };

            provinceList.AddRange(entities.Province.ToList());

            return provinceList;
        }

        [HttpPost]
        [Authorize]
        public int AdminUserOrderRowStatus(string id) // 1 = normal 2 = not order in last 5 days 3 = never order
        {
            var userId = Int32.Parse(id);
            var user = entities.User.FirstOrDefault(u => u.UserID == userId);

            if (user != null)
            {
                //only dropship
                if (user.UserType == 2)
                {
                    var lastOrder = entities.Order.Where(o => o.UserID == userId).OrderByDescending(o => o.OrderDateTime).FirstOrDefault();


                    if (lastOrder != null)
                    {
                        if (lastOrder.OrderDateTime > DateTime.Now.AddDays(-7))
                        {
                            return 1;
                        }
                        else
                        {
                            return 2;
                        }
                    }
                    else
                    {
                        return 3;
                    }
                }
                else
                {
                    return 1;
                }
            }

            return 1;
        }

        [Authorize]
        public JsonResult AdminUserTableData()
        {
            var user = entities.User.Select(x => new
            {
                x.UserName,
                x.ShopName,
                x.LineID,
                x.Phone,
                x.Email,
                x.Province,
                x.City,
                x.Address,
                x.Website,
                x.UserType,
                x.MemberCode
            }).ToList();

            var list =
                user.Select(
                    x =>
                        new[]
                        {
                            x.UserName, x.ShopName, x.LineID, x.Phone.ToString(), x.Email, x.Province, x.City, x.Address,
                            x.Website, x.UserType.ToString(), x.MemberCode
                        }).ToList();

            var dataTable = new { data = list };

            // Return JSON object
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public string AdminUserTableUpdate(string id, string value)
        {
            var tokenId = id.Split('_');

            var userId = int.Parse(tokenId[2]);

            var user = entities.User.FirstOrDefault(u => u.UserID == userId);

            if (user != null)
            {
                switch (tokenId[1])
                {
                    case "username":
                        user.UserName = value;
                        break;
                    case "shopname":
                        user.ShopName = value;
                        break;
                    case "lineid":
                        user.LineID = value;
                        break;
                    case "phone":
                        user.Phone = value;
                        break;
                    case "email":
                        user.Email = value;
                        break;
                    case "province":
                        user.Province = value;
                        break;
                    case "city":
                        user.City = value;
                        break;
                    case "address":
                        user.Address = value;
                        break;
                    case "website":
                        user.Website = value;
                        break;
                    case "usertype":
                        user.UserType = int.Parse(value);
                        break;
                    case "membercode":
                        user.MemberCode = value;
                        break;
                }

                entities.SaveChanges();

                return value;
            }

            return value;
        }

        [Authorize]
        [HttpPost]
        public string AdminOrderTableUpdate(string id, string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var tokenId = id.Split('_');

                    var amount = int.Parse(value);

                    var productId = int.Parse(tokenId[2]);

                    var orderNumber = tokenId[3];

                    var order = entities.Order.FirstOrDefault(u => u.OrderNumber == orderNumber && u.ProductID == productId);
                    if (order == null)
                    {
                        order = entities.Order.FirstOrDefault(u => u.OrderNumber == orderNumber);
                    }

                    if (order != null)
                    {
                        var originalPrice = order.Price.Value;
                        var totalAmount = (order.AmountXS ?? 0) + (order.AmountS ?? 0) + (order.AmountM ?? 0) + (order.AmountL ?? 0) + (order.AmountXL ?? 0) + (order.AmountF ?? 0);
                        var originalEach = originalPrice / totalAmount;

                        switch (tokenId[1])
                        {
                            case "amountxs":
                                totalAmount -= order.AmountXS.Value;
                                order.AmountXS = amount;
                                break;
                            case "amounts":
                                totalAmount -= order.AmountS.Value;
                                order.AmountS = amount;
                                break;
                            case "amountm":
                                totalAmount -= order.AmountM.Value;
                                order.AmountM = amount;
                                break;
                            case "amountl":
                                totalAmount -= order.AmountL.Value;
                                order.AmountL = amount;
                                break;
                            case "amountxl":
                                totalAmount -= order.AmountXL.Value;
                                order.AmountXL = amount;
                                break;
                            case "amountf":
                                totalAmount -= order.AmountF.Value;
                                order.AmountF = amount;
                                break;
                            case "discount":
                                order.Discount = amount;
                                break;
                            case "shipping":
                                order.Shipping = amount;
                                order.Status = 3;
                                break;
                        }

                        if (productId != 0)
                        {
                            var product = entities.Product.FirstOrDefault(p => p.ProductID == productId);
                            var productCategory = entities.Category.FirstOrDefault(c => c.CategoryID == product.CategoryID);
                            totalAmount += amount;
                            var user = entities.User.FirstOrDefault(u => u.UserID == order.UserID);
                            var userType = 0;
                            if (user != null)
                            {
                                userType = user.UserType.Value;
                            }
                            var newPrice = originalEach * totalAmount;

                            if ((product.Name.Contains("แขนสั้น") || productCategory.Name.Contains("แขนสั้น") || productCategory.Description.Contains("แขนสั้น")) && totalAmount < 50)
                            {
                                newPrice = totalAmount * 130;
                            }
                            else if (userType == 2)
                            {
                                newPrice = totalAmount * (product.Price ?? 0);
                            }
                            else if (userType == 5)
                            {
                                newPrice = totalAmount * (product.Price4 ?? 0);
                            }
                            else
                            {
                                if (totalAmount >= 50)
                                {
                                    newPrice = totalAmount * (product.Price3 ?? 0);
                                }
                                else
                                {
                                    newPrice = totalAmount * (product.Price2 ?? 0);
                                }
                            }


                            order.Price = newPrice;
                        }

                        entities.SaveChanges();

                        return amount.ToString();
                    }
                }
            }
            catch (Exception)
            {
                return "0";
            }
            return "0";
        }
        [Authorize]
        [HttpPost]
        public ActionResult ApproveUserRow(string id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(id))
            {
                var userId = int.Parse(id);
                var user = entities.User.FirstOrDefault(u => u.UserID == userId);
                if (user != null)
                {
                    switch (user.UserType.Value)
                    {
                        case 2:
                            var todayYearMonth = DateTime.Now.ToString("yyMM");
                            var latestDropshipUser = entities.User.Where(u => u.MemberCode.StartsWith(todayYearMonth) && u.UserType == 2).OrderByDescending(u => u.MemberCode).FirstOrDefault();
                            string nextNumber2;
                            if (latestDropshipUser != null)
                            {
                                var lastNumber = Convert.ToInt32(latestDropshipUser.MemberCode.Substring(6, 3));
                                lastNumber += 1;
                                nextNumber2 = lastNumber.ToString("000");
                            }
                            else
                            {
                                nextNumber2 = "001";
                            }
                            user.MemberCode = string.Format("{0}0_{1}", todayYearMonth, nextNumber2);
                            user.Password = SHA1.Encode(user.Phone);
                            user.FirstLog = true;
                            break;
                        case 3:
                            var province = entities.Province.FirstOrDefault(p => p.Name == user.Province);
                            var provinceCode = province.Code;
                            var provinceNumber = user.Province == "กรุงเทพมหานคร" ? "1" : "2";
                            if (provinceNumber == "2")
                            {
                                provinceNumber = user.City.Contains("เมือง") ? "1" : "2";
                            }
                            var latestWholesaleUser = entities.User.Where(u => u.MemberCode.StartsWith(provinceCode + provinceNumber)).OrderByDescending(u => u.MemberCode).FirstOrDefault();
                            string nextNumber3;
                            if (latestWholesaleUser != null)
                            {
                                var lastNumber = Convert.ToInt32(latestWholesaleUser.MemberCode.Substring(4, 3));
                                lastNumber += 1;
                                nextNumber3 = lastNumber.ToString("000");
                            }
                            else
                            {
                                nextNumber3 = "001";
                            }

                            user.MemberCode = string.Format("{0}{1}{2}", provinceCode, provinceNumber, nextNumber3);
                            user.Password = SHA1.Encode(user.Phone);
                            user.FirstLog = true;
                            break;
                        case 4:
                            var province2 = entities.Province.FirstOrDefault(p => p.Name == user.Province);
                            var provinceCode2 = province2.Code;
                            var provinceNumber2 = user.Province == "กรุงเทพมหานคร" ? "1" : "2";
                            if (provinceNumber2 == "2")
                            {
                                provinceNumber = user.City.Contains("เมือง") ? "1" : "2";
                            }
                            var latestWholesaleUser2 = entities.User.Where(u => u.MemberCode.StartsWith(provinceCode2 + provinceNumber2)).OrderByDescending(u => u.MemberCode).FirstOrDefault();
                            string nextNumber4;
                            if (latestWholesaleUser2 != null)
                            {
                                var lastNumber = Convert.ToInt32(latestWholesaleUser2.MemberCode.Substring(4, 3));
                                lastNumber += 1;
                                nextNumber4 = lastNumber.ToString("000");
                            }
                            else
                            {
                                nextNumber4 = "001";
                            }

                            user.MemberCode = string.Format("{0}{1}{2}", provinceCode2, provinceNumber2, nextNumber4);
                            user.Password = SHA1.Encode(user.Phone);
                            user.FirstLog = true;
                            break;

                    }
                    user.Approved = true;
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminUser"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult DenyUserRow(string id, string reason)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(id))
            {
                var userId = int.Parse(id);
                var user = entities.User.FirstOrDefault(u => u.UserID == userId);
                if (user != null)
                {
                    user.Approved = false;
                    user.Remark = reason;
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminUser"));
        }


        [Authorize]
        public ActionResult GetCategory()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }
            var categoryKeyPair = new Dictionary<string, string>();

            var categories = entities.Category.ToList();

            foreach (var category in categories)
            {
                categoryKeyPair.Add(category.CategoryID.ToString(), category.Name);
            }

            return Json(categoryKeyPair, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult GetCategoryName(string id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(id))
            {
                var categoryId = int.Parse(id);
                var category = entities.Category.FirstOrDefault(c => c.CategoryID == categoryId);
                if (category != null)
                {
                    return Content(category.Name);
                }
            }

            return Content(string.Empty);
        }

        [Authorize]
        public string GetProductSortID(string pid)
        {
            if (!string.IsNullOrEmpty(pid))
            {
                var productId = int.Parse(pid);
                var product = entities.Product.FirstOrDefault(p => p.ProductID == productId);

                if (product != null)
                {
                    return product.SortID.Value.ToString("D3");
                }
            }

            return string.Empty;
        }

        [Authorize]
        public ActionResult GetCategorySortID(string cid)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(cid))
            {
                var categoryId = int.Parse(cid);
                var category = entities.Category.FirstOrDefault(c => c.CategoryID == categoryId);

                if (category != null)
                {
                    return Content(category.SortID.Value.ToString("D2"));
                }
            }

            return Content(string.Empty);
        }

        [Authorize]
        public ActionResult GetProductAndCategorySortID(string pid)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(pid))
            {
                var productId = int.Parse(pid);
                var product = entities.Product.FirstOrDefault(p => p.ProductID == productId);
                var category = entities.Category.FirstOrDefault(c => c.CategoryID == product.CategoryID);

                if (product != null)
                {
                    return Content(category.SortID.Value.ToString("D2") + product.SortID.Value.ToString("D3"));
                }
            }

            return Content(string.Empty);
        }

        [Authorize]
        public ActionResult GetBranchName(string id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(id))
            {
                var branchId = int.Parse(id);
                var branch = entities.Branch.FirstOrDefault(b => b.BranchID == branchId);
                if (branch != null)
                {
                    return Content(branch.Name);
                }
            }

            return Content(string.Empty);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ClearShoppingCart()
        {
            Session["ShoppingCart"] = null;

            return Json(Url.Action("OrderProduct", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult AdminClearShoppingCart()
        {
            Session["ShoppingCart"] = null;

            return Json(Url.Action("AdminOrderProduct", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateBranch(string branchName)
        {
            if (entities.Branch.FirstOrDefault(b => b.Name == branchName) != null)
            {
                return RedirectToAction("AdminBranch");
            }

            entities.Branch.Add(
                new Branch
                {
                    Name = branchName
                }
            );

            entities.SaveChanges();

            var branchID = entities.Branch.FirstOrDefault(b => b.Name == branchName).BranchID;
            var products = entities.Product.Where(p => p.BranchID == 1).ToList();

            foreach (var product in products)
            {
                entities.Product.Add(new Product
                {
                    Barcode = product.Barcode,
                    Name = product.Name,
                    Image = product.Image,
                    AmountXS = product.AmountXS,
                    AmountS = product.AmountS,
                    AmountM = product.AmountM,
                    AmountL = product.AmountL,
                    AmountXL = product.AmountXL,
                    AmountF = product.AmountF,
                    CategoryID = product.CategoryID,
                    Price = product.Price,
                    Price2 = product.Price2,
                    Price3 = product.Price3,
                    Price4 = product.Price4,
                    PriceVIP = product.PriceVIP,
                    BranchID = branchID,
                    ImageFileName = product.ImageFileName,
                    ImageContentType = product.ImageContentType,
                    SortID = product.SortID
                });
            }

            entities.SaveChanges();

            return RedirectToAction("AdminBranch");
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateSupplier(string supplierName, string remark, string code, string phone)
        {
            entities.Supplier.Add(
                new Supplier
                {
                    Name = supplierName,
                    Code = code,
                    Phone = phone,
                    Remark = remark
                });
            entities.SaveChanges();

            return RedirectToAction("AdminSupplier");
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateProduct(string Barcode, string Name, HttpPostedFileBase Image, string AmountXS, string AmountS, string AmountM,
            string AmountL, string AmountXL, string AmountF, string Category, string Price, string Price2, string Price3, string Price4, string PriceVIP, string sortID)
        {
            if (Image != null)
            {
                using (var reader = new System.IO.BinaryReader(Image.InputStream))
                {
                    var imageContent = reader.ReadBytes(Image.ContentLength);
                    foreach (var branch in entities.Branch)
                    {
                        entities.Product.Add(new Product
                        {
                            Barcode = Barcode,
                            Name = Name,
                            Image = imageContent,
                            AmountXS = int.Parse(AmountXS),
                            AmountS = int.Parse(AmountS),
                            AmountM = int.Parse(AmountM),
                            AmountL = int.Parse(AmountL),
                            AmountXL = int.Parse(AmountXL),
                            AmountF = int.Parse(AmountF),
                            CategoryID = int.Parse(Category),
                            Price = int.Parse(Price),
                            Price2 = int.Parse(Price2),
                            Price3 = int.Parse(Price3),
                            Price4 = int.Parse(Price4),
                            PriceVIP = int.Parse(PriceVIP),
                            BranchID = branch.BranchID,
                            ImageFileName = Image.FileName,
                            ImageContentType = Image.ContentType,
                            SortID = int.Parse(sortID)
                        });
                    }
                }
            }
            else
            {
                foreach (var branch in entities.Branch)
                {
                    entities.Product.Add(new Product
                    {
                        Barcode = Barcode,
                        Name = Name,
                        AmountXS = int.Parse(AmountXS),
                        AmountS = int.Parse(AmountS),
                        AmountM = int.Parse(AmountM),
                        AmountL = int.Parse(AmountL),
                        AmountXL = int.Parse(AmountXL),
                        AmountF = int.Parse(AmountF),
                        CategoryID = int.Parse(Category),
                        Price = int.Parse(Price),
                        Price2 = int.Parse(Price2),
                        Price3 = int.Parse(Price3),
                        Price4 = int.Parse(Price4),
                        PriceVIP = int.Parse(PriceVIP),
                        BranchID = branch.BranchID,
                        SortID = int.Parse(sortID)
                    });
                }
            }



            entities.SaveChanges();

            return RedirectToAction("AdminProduct", "Main");
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateUser(string UserName, string ShopName, string LineID, string Phone, string Email,
            string Province, string CityList, string Address, string Website, string UserType, string MemberCode,
            string Password)
        {
            var provinceID = int.Parse(Province);
            var cityID = int.Parse(CityList);
            var province = entities.Province.FirstOrDefault(p => p.ProvinceID == provinceID);
            var city = entities.City.FirstOrDefault(c => c.CityID == cityID);
            entities.User.Add(new User
            {
                UserName = UserName,
                ShopName = ShopName,
                LineID = LineID,
                Phone = Phone,
                Email = Email,
                Province = province.Name,
                City = city.Name,
                Address = Address,
                Website = Website,
                UserType = int.Parse(UserType),
                MemberCode = MemberCode,
                Password = SHA1.Encode(Password),
                RegisteredDate = DateTime.Now,
                Approved = true,
                FirstLog = false
            });
            entities.SaveChanges();

            return RedirectToAction("AdminUser", "Main");
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteBranchRow(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var branchId = int.Parse(tokenId[1]);
                var branch = entities.Branch.FirstOrDefault(b => b.BranchID == branchId);
                if (branch != null)
                {
                    entities.Branch.Remove(branch);
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminBranch", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteSupplierRow(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var supplierID = int.Parse(tokenId[1]);
                var supplier = entities.Supplier.FirstOrDefault(b => b.SupplierID == supplierID);
                if (supplier != null)
                {
                    entities.Supplier.Remove(supplier);
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminSupplier", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteProductRow(string id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var productId = int.Parse(tokenId[1]);
                var product = entities.Product.FirstOrDefault(p => p.ProductID == productId);
                if (product != null)
                {
                    entities.Product.Remove(product);
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminProduct", "Main"));
        }

        [Authorize]
        public ActionResult DisplayImage(int productId)
        {
            var product = entities.Product.Find(productId);
            if (product != null && product.Image != null)
            {
                return File(product.Image, product.ImageContentType);
            }
            return null;
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteShoppingCart(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var productId = int.Parse(tokenId[1]);

                var shopItems = Session["ShoppingCart"] as List<Product>;

                if (shopItems != null)
                {
                    shopItems.Remove(shopItems.FirstOrDefault(i => i.ProductID == productId));
                }

                if (shopItems.Count == 0)
                {
                    Session["ShoppingCart"] = null;
                }
                else
                {
                    Session["ShoppingCart"] = shopItems;
                }
            }

            UpdateShoppingCart();

            return Json(Url.Action("OrderProduct", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult AdminDeleteShoppingCart(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var productId = int.Parse(tokenId[1]);

                var shopItems = Session["ShoppingCart"] as List<Product>;

                if (shopItems != null)
                {
                    shopItems.Remove(shopItems.FirstOrDefault(i => i.ProductID == productId));
                }

                if (shopItems.Count == 0)
                {
                    Session["ShoppingCart"] = null;
                }
                else
                {
                    Session["ShoppingCart"] = shopItems;
                }
            }

            UpdateShoppingCart();

            return Json(Url.Action("AdminOrderProduct", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteUserRow(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var userId = int.Parse(tokenId[1]);
                var user = entities.User.FirstOrDefault(u => u.UserID == userId);
                if (user != null)
                {
                    entities.User.Remove(user);
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminUser", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult DisableUserRow(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var userId = int.Parse(tokenId[1]);
                var user = entities.User.FirstOrDefault(u => u.UserID == userId);
                if (user != null)
                {
                    user.Approved = false;
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminUserOrder", "Main"));
        }

        [Authorize]
        [HttpPost]
        public ActionResult EnableUserRow(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var userId = int.Parse(tokenId[1]);
                var user = entities.User.FirstOrDefault(u => u.UserID == userId);
                if (user != null)
                {
                    user.Approved = true;
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminUserOrder", "Main"));
        }

        [Authorize]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var userType = int.Parse(Session["UserType"].ToString());

            switch (userType)
            {
                case 1:
                    return RedirectToAction("AdminUser");
                case 2:
                    return RedirectToAction("OrderStatus");
                case 3:
                    return RedirectToAction("OrderStatus");
                case 4:
                    return RedirectToAction("OrderStatus");
                case 5:
                    return RedirectToAction("OrderStatus");
                case 6:
                    return RedirectToAction("OrderStatus");
                case 9:
                    return RedirectToAction("AdminUserOrder");
                default:
                    return View();
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [Authorize]
        public string BatchGeneratePassword()
        {
            try
            {
                var users = entities.User.Where(u => u.Password == null || u.Password == "").ToList();

                foreach (var user in users)
                {
                    user.Password = SHA1.Encode(user.Phone.Replace("+", "").Replace("-", "").Replace(" ", ""));
                }

                entities.SaveChanges();
            }
            catch (Exception)
            {
                return "FAILED";
            }

            return "SUCCESS";
        }

        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            if (ModelState.IsValid)
            {
                var encodedPass = SHA1.Encode(password);

                var user = entities.User.FirstOrDefault(u => u.UserName == userName);

                if (user != null)
                {
                    if (user.FirstLog.HasValue && user.FirstLog.Value && user.Password == encodedPass)
                    {
                        Session["UserFirstLogID"] = user.UserID;
                        return RedirectToAction("ChangePassword", "Main");
                    }
                    else if (user.Password == encodedPass)
                    {
                        if (user.Approved.HasValue && user.Approved.Value)
                        {
                            Session["LoginMessage"] = "";
                            FormsAuthentication.SetAuthCookie(user.UserName, false);
                            Session["UserID"] = user.UserID;
                            Session["UserType"] = user.UserType;
                            Session["ShopName"] = user.ShopName;
                            Session["Phone"] = user.Phone;
                            Session["Address"] = user.Address;
                            return RedirectToAction("Index", "Main");
                        }
                        else
                        {
                            Session["LoginMessage"] = "บัญชีผู้ใช้ของท่านถูกปิดอยู่ กรุณาติดต่อผู้ดูแล";
                            return RedirectToAction("Login", "Main");
                        }
                    }
                    else if (user.Password == null)
                    {
                        Session["LoginMessage"] = "บัญชีผู้ใช้ของท่านยังไม่ได้รับการอนุมัติ กรุณารอ 2-3 วันทำการหลังจากสมัคร";
                        return RedirectToAction("Login", "Main");
                    }
                }

                Session["LoginMessage"] = "ข้อมูลไม่ถูกต้อง กรุณาตรวจสอบชื่อผู้ใช้หรือพาสเวิร์ด";
                ModelState.AddModelError("", "Login data is incorrect!");
            }
            return RedirectToAction("Login", "Main");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Main");
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(string password, string confirm)
        {
            if (Session["UserFirstLogID"] == null)
            {
                Session["ChangePasswordMessage"] = "ไม่พบผู้ใช้ในระบบ กรุณาทำการล็อกอินใหม่";
                return View();
            }

            if (password != confirm)
            {
                Session["ChangePasswordMessage"] = "รหัสผ่านทั้งสองช่องไม่ตรงกัน กรูณาตรวจสอบอีกครั้ง";
                return View();
            }

            if (password.Length < 5)
            {
                Session["ChangePasswordMessage"] = "รหัสผ่านต้องมีอย่างน้อย 5 ตัวอักษรหรือตัวเลข";
                return View();
            }

            var userID = int.Parse(Session["UserFirstLogID"].ToString());
            var user = entities.User.FirstOrDefault(u => u.UserID == userID);

            if (user != null)
            {
                user.Password = SHA1.Encode(password);
                user.FirstLog = false;

                entities.SaveChanges();
                Session["UserFirstLogID"] = null;
                Session["ChangePasswordMessage"] = null;
                Session["LoginMessage"] = "บันทึกรหัสผ่านใหม่เรียบร้อยแล้ว กรุณาล็อกอินอีกครั้งด้วยรหัสผ่านใหม่";

            }
            else
            {
                Session["ChangePasswordMessage"] = "ไม่พบผู้ใช้นี้ในระบบ กรุณาติดต่อผู้ดูแลระบบ";
                return View();
            }

            return RedirectToAction("Login", "Main");
        }

        [Authorize]
        public ActionResult OrderDetail(string orderNumber, bool notify = false)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (notify)
            {
                Session["OrderCompleted"] = "true";
            }
            else
            {
                Session["OrderCompleted"] = null;
            }
            var userId = int.Parse(Session["UserID"].ToString());
            var userType = int.Parse(Session["UserType"].ToString());

            var products = new List<Product>();
            var orderList = entities.Order.Where(o => o.OrderNumber == orderNumber).ToList();

            foreach (var orderItem in orderList)
            {
                var productItem = entities.Product.FirstOrDefault(p => p.ProductID == orderItem.ProductID.Value);
                products.Add(new Product
                {
                    AmountF = orderItem.AmountF,
                    AmountL = orderItem.AmountL,
                    AmountM = orderItem.AmountM,
                    AmountS = orderItem.AmountS,
                    AmountXL = orderItem.AmountXL,
                    AmountXS = orderItem.AmountXS,
                    Price = orderItem.Price,
                    ProductID = orderItem.ProductID.Value,
                    Name = productItem.Name,
                    Barcode = productItem.Barcode
                });
            }

            //var products =
            //    entities.Order.Where(o => o.OrderNumber == orderNumber)
            //        .Select(o => entities.Product.FirstOrDefault(p => p.ProductID == o.ProductID)).ToList();
            var order = entities.Order.FirstOrDefault(o => o.OrderNumber == orderNumber);


            if (order == null || (order.UserID.HasValue && order.UserID.Value != userId && userType != 1))
            {
                return RedirectToAction("Login");
            }

            var orderUserID = order.OrderUserID.HasValue ? order.OrderUserID.Value : order.UserID.Value;
            var orderUser = entities.User.FirstOrDefault(u => u.UserID == orderUserID);
            var orderUserType = orderUser.UserType.Value;

            var orders = new _OrderDetail
            {
                ProductID = order.ProductID,
                OrderNumber = order.OrderNumber,
                DeliverDateTime = order.DeliverDateTime,
                DeliveryAddress = order.DeliveryAddress,
                DeliveryName = order.DeliveryName,
                EMSNumber = order.EMSNumber,
                OrderDateTime = order.OrderDateTime,
                ProcessDateTime = order.ProcessDateTime,
                Remark = order.Remark,
                Status = order.Status,
                UserID = order.UserID,
                Products = products,
                OrderUserID = orderUserID,
                UserType = userType,
                Discount = order.Discount,
                Shipping = order.Shipping
            };

            return View(orders);
        }

        [Authorize]
        [HttpPost]
        public ActionResult OrderDetailRow(string orderNumber)
        {
            return Json(Url.Action("OrderDetail", new { orderNumber }));
        }

        [Authorize]
        public ActionResult OrderProduct()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var product = entities.Product.Where(p => p.BranchID == 1).OrderBy(p => p.SortID).ToList();

            return View(product);
        }

        [Authorize]
        public ActionResult OrderStatus()
        {
            if (Session["UserID"] != null)
            {
                var userId = int.Parse(Session["UserID"].ToString());
                var order =
                    entities.Order.Where(o => o.UserID == userId)
                        .GroupBy(o => o.OrderNumber)
                        .Select(o => new _OrderStatus
                        {
                            OrderNumber = o.Key,
                            OrderUserID = o.FirstOrDefault().OrderUserID,
                            DeliveryAddress = o.FirstOrDefault().DeliveryAddress,
                            Remark = o.FirstOrDefault().Remark,
                            DeliveryName = o.FirstOrDefault().DeliveryName,
                            OrderDate = o.FirstOrDefault().OrderDateTime,
                            Status = o.FirstOrDefault().Status
                        });

                return View(order.ToList());
            }

            return RedirectToAction("Login", "Main");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SubmitShoppingCart(string deliveryName, string deliveryAddress, int phone)
        {
            if (Session["UserID"] != null)
            {
                var shopItems = Session["ShoppingCart"] as List<Product>;
                var orderDateTime = DateTime.Now;
                //var batchRef = Guid.NewGuid();
                var orderSeedNo = (entities.Order.Any() ? entities.Order.Max(o => o.OrderID) : 571) + 432;
                //generate order no.
                var ordernumber = string.Format("{0}{1}", DateTime.Now.ToString("yyMMdd"), orderSeedNo.ToString("0000"));

                if (shopItems != null)
                {
                    var userId = int.Parse(Session["UserID"].ToString());
                    foreach (var item in shopItems)
                    {
                        entities.Order.Add(new Order
                        {
                            ProductID = item.ProductID,
                            UserID = userId,
                            OrderNumber = ordernumber,
                            DeliveryName = deliveryName,
                            DeliveryAddress = deliveryAddress,
                            AmountXS = item.AmountXS,
                            AmountS = item.AmountS,
                            AmountM = item.AmountM,
                            AmountL = item.AmountL,
                            AmountXL = item.AmountXL,
                            AmountF = item.AmountF,
                            //BatchRef = batchRef,
                            OrderDateTime = orderDateTime,
                            Status = 1,
                            DeliveryPhone = phone,
                            Price = item.Price,
                            Shipping = 0,
                            Discount = 0
                        });
                    }

                    entities.SaveChanges();
                    Session["ShoppingCart"] = null;
                }

                return RedirectToAction("OrderDetail", new { orderNumber = ordernumber, notify = true });
            }

            return RedirectToAction("Login");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SubmitImpersonatedShoppingCart(string deliveryName, string deliveryAddress, int phone)
        {
            if (Session["UserID"] != null && Session["impersonateUserID"] != null)
            {
                var shopItems = Session["ShoppingCart"] as List<Product>;
                var orderDateTime = DateTime.Now;
                //var batchRef = Guid.NewGuid();
                var orderSeedNo = (entities.Order.Any() ? entities.Order.Max(o => o.OrderID) : 571) + 432;
                //generate order no.
                var ordernumber = string.Format("{0}{1}", DateTime.Now.ToString("yyMMdd"), orderSeedNo.ToString("0000"));

                if (shopItems != null)
                {
                    var userId = int.Parse(Session["impersonateUserID"].ToString());
                    var orderUserId = int.Parse(Session["UserID"].ToString());
                    foreach (var item in shopItems)
                    {
                        entities.Order.Add(new Order
                        {
                            ProductID = item.ProductID,
                            UserID = userId,
                            OrderUserID = orderUserId,
                            OrderNumber = ordernumber,
                            DeliveryName = deliveryName,
                            DeliveryAddress = deliveryAddress,
                            AmountXS = item.AmountXS,
                            AmountS = item.AmountS,
                            AmountM = item.AmountM,
                            AmountL = item.AmountL,
                            AmountXL = item.AmountXL,
                            AmountF = item.AmountF,
                            //BatchRef = batchRef,
                            OrderDateTime = orderDateTime,
                            Status = 1,
                            DeliveryPhone = phone,
                            Price = item.Price
                        });
                    }

                    entities.SaveChanges();
                    Session["ShoppingCart"] = null;
                }

                return RedirectToAction("OrderDetail", new { orderNumber = ordernumber, notify = true });
            }

            return RedirectToAction("Login");
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateCategory(string Name, string Description, int sortID, bool SizeXS = false, bool SizeS = false, bool SizeM = false, bool SizeL = false, bool SizeXL = false, bool SizeXXL = false)
        {
            entities.Category.Add(new Category
            {
                Name = Name,
                Description = Description,
                XS = SizeXS,
                S = SizeS,
                M = SizeM,
                L = SizeL,
                XL = SizeXL,
                XXL = SizeXXL,
                SortID = sortID
            });
            entities.SaveChanges();

            return RedirectToAction("AdminCategory", "Main");
        }

        [Authorize]
        [HttpPost]
        public string AdminCategoryTableUpdate(string id, string value)
        {
            var tokenId = id.Split('_');

            var categoryId = int.Parse(tokenId[2]);

            var category = entities.Category.FirstOrDefault(u => u.CategoryID == categoryId);

            if (category != null)
            {
                switch (tokenId[1])
                {
                    case "name":
                        category.Name = value;
                        break;
                    case "description":
                        category.Description = value;
                        break;
                    case "sortid":
                        int sortID;
                        if (int.TryParse(value, out sortID))
                        {
                            if (sortID > 0 && sortID < 100)
                            {
                                category.SortID = sortID;
                            }
                            else return category.SortID.Value.ToString("D3");
                        }
                        else return category.SortID.Value.ToString("D3");
                        break;
                }

                entities.SaveChanges();

                return value;
            }

            return value;
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteCategoryRow(string id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            if (!string.IsNullOrEmpty(id))
            {
                var tokenId = id.Split('_');
                var categoryId = int.Parse(tokenId[1]);
                var category = entities.Category.FirstOrDefault(p => p.CategoryID == categoryId);
                if (category != null)
                {
                    entities.Category.Remove(category);
                    entities.SaveChanges();
                }
            }

            return Json(Url.Action("AdminCategory", "Main"));
        }

        [Authorize]
        [HttpPost]
        public void UpdateCategorySizeRow(string id, bool value)
        {
            var tokenId = id.Split('_');

            var categoryId = int.Parse(tokenId[2]);

            var size = tokenId[1];

            var category = entities.Category.FirstOrDefault(u => u.CategoryID == categoryId);

            if (category != null)
            {
                switch (tokenId[1])
                {
                    case "XS":
                        category.XS = value;
                        break;
                    case "S":
                        category.S = value;
                        break;
                    case "M":
                        category.M = value;
                        break;
                    case "L":
                        category.L = value;
                        break;
                    case "XL":
                        category.XL = value;
                        break;
                    case "XXL":
                        category.XXL = value;
                        break;
                }

                entities.SaveChanges();
            }
        }

        [Authorize]
        public ActionResult MinimumStockSetting()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login");
            }

            var setting = new List<_MinimumStock>();

            var products = entities.Product.ToList();
            var stockSettings = entities.StockSetting.ToList();
            var branches = entities.Branch.ToList();
            var categories = entities.Category.ToList();

            foreach (var product in products)
            {
                var stockSetting = stockSettings.FirstOrDefault(s => s.BranchID == product.BranchID && s.ProductID == product.ProductID);
                if (stockSetting == null)
                {
                    stockSetting = entities.StockSetting.Add(new StockSetting
                    {
                        BranchID = product.BranchID.Value,
                        ProductID = product.ProductID,
                        MinL = 0,
                        MinM = 0,
                        MinS = 0,
                        MinXL = 0,
                        MinXS = 0,
                        MinXXL = 0
                    });
                }

                setting.Add(new _MinimumStock
                {
                    Product = product,
                    Branch = branches.FirstOrDefault(b => b.BranchID == product.BranchID.Value),
                    Category = categories.FirstOrDefault(c => c.CategoryID == product.CategoryID),
                    StockSetting = stockSetting
                });
            }

            entities.SaveChanges();

            return View(setting);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UploadMinimumStockSetting(HttpPostedFileBase file)
        {
            ViewBag.excelError = null;
            try
            {
                using (var package = new ExcelPackage(file.InputStream))
                {
                    var workbook = package.Workbook;

                    var worksheet = workbook.Worksheets.First();

                    for (var i = 2; i <= worksheet.Dimension.End.Row; i++)
                    {
                        var branchId = GetBranchID(worksheet.Cells[i, 1].Value.ToString());
                        if (branchId == 0) continue;
                        var productId = int.Parse(worksheet.Cells[i, 2].Value.ToString().Trim());
                        var minXS = int.Parse(worksheet.Cells[i, 5].Value.ToString().Trim());
                        var minS = int.Parse(worksheet.Cells[i, 6].Value.ToString().Trim());
                        var minM = int.Parse(worksheet.Cells[i, 7].Value.ToString().Trim());
                        var minL = int.Parse(worksheet.Cells[i, 8].Value.ToString().Trim());
                        var minXL = int.Parse(worksheet.Cells[i, 9].Value.ToString().Trim());
                        var minXXL = int.Parse(worksheet.Cells[i, 10].Value.ToString().Trim());

                        var stockSetting = entities.StockSetting.FirstOrDefault(s => s.ProductID == productId);
                        if (stockSetting == null) continue;
                        stockSetting.MinXS = minXS;
                        stockSetting.MinS = minS;
                        stockSetting.MinM = minM;
                        stockSetting.MinL = minL;
                        stockSetting.MinXL = minXL;
                        stockSetting.MinXXL = minXXL;

                        entities.SaveChanges();
                    }
                }
            }
            catch (IOException)
            {
                ViewBag.excelError = "ไฟล์ Excel ที่นำเข้ามาถูกเปิดใช้งานโดยโปรแกรมอื่นอยู่";
            }

            return RedirectToAction("MinimumStockSetting");
        }

        [Authorize]
        [HttpPost]
        public string AdminMinimumStockUpdate(string id, string value)
        {
            var tokenId = id.Split('_');
            var intValue = 0;

            int.TryParse(value, out intValue);

            var combinedId = tokenId[2].Split('+');

            var productId = int.Parse(combinedId[1]);
            var branchId = int.Parse(combinedId[0]);

            var stockSetting = entities.StockSetting.FirstOrDefault(s => s.ProductID == productId && s.BranchID == branchId);

            if (stockSetting != null)
            {
                switch (tokenId[1])
                {
                    case "xs":
                        stockSetting.MinXS = intValue;
                        break;
                    case "s":
                        stockSetting.MinS = intValue;
                        break;
                    case "m":
                        stockSetting.MinM = intValue;
                        break;
                    case "l":
                        stockSetting.MinL = intValue;
                        break;
                    case "xl":
                        stockSetting.MinXL = intValue;
                        break;
                    case "xxl":
                        stockSetting.MinXXL = intValue;
                        break;
                }

                entities.SaveChanges();

                return value;
            }

            return value;
        }

        [Authorize]
        [HttpPost]
        public ActionResult DownloadStockSetting()
        {
            var stockSettings = entities.StockSetting.ToList();

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;
            //Header of table  
            //  
            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;
            workSheet.Cells[1, 1].Value = "Branch";
            workSheet.Cells[1, 2].Value = "ProductID";
            workSheet.Cells[1, 3].Value = "Name";
            workSheet.Cells[1, 4].Value = "Category";
            workSheet.Cells[1, 5].Value = "XS";
            workSheet.Cells[1, 6].Value = "S";
            workSheet.Cells[1, 7].Value = "M";
            workSheet.Cells[1, 8].Value = "L";
            workSheet.Cells[1, 9].Value = "XL";
            workSheet.Cells[1, 10].Value = "XXL";
            //Body of table  
            //  
            int recordIndex = 2;
            foreach (var stockSetting in stockSettings)
            {
                var product = entities.Product.FirstOrDefault(p => p.ProductID == stockSetting.ProductID);
                if (product == null) continue;
                var category = entities.Category.FirstOrDefault(c => c.CategoryID == product.CategoryID);
                if (category == null) continue;
                var branch = entities.Branch.FirstOrDefault(b => b.BranchID == product.BranchID);
                if (branch == null) continue;

                workSheet.Cells[recordIndex, 1].Value = branch.Name;
                workSheet.Cells[recordIndex, 2].Value = stockSetting.ProductID;
                workSheet.Cells[recordIndex, 3].Value = product.Name;
                workSheet.Cells[recordIndex, 4].Value = category.Name;
                workSheet.Cells[recordIndex, 5].Value = stockSetting.MinXS;
                workSheet.Cells[recordIndex, 6].Value = stockSetting.MinS;
                workSheet.Cells[recordIndex, 7].Value = stockSetting.MinM;
                workSheet.Cells[recordIndex, 8].Value = stockSetting.MinL;
                workSheet.Cells[recordIndex, 9].Value = stockSetting.MinXL;
                workSheet.Cells[recordIndex, 10].Value = stockSetting.MinXXL;
                recordIndex++;
            }
            workSheet.Column(1).AutoFit();
            workSheet.Column(2).AutoFit();
            workSheet.Column(3).AutoFit();
            workSheet.Column(4).AutoFit();
            workSheet.Column(5).AutoFit();
            workSheet.Column(6).AutoFit();
            workSheet.Column(7).AutoFit();
            workSheet.Column(8).AutoFit();
            workSheet.Column(9).AutoFit();
            workSheet.Column(10).AutoFit();
            string excelName = "MinimumStockSettings";
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }

            return RedirectToAction("MinimumStockSetting");
        }

        [Authorize]
        [HttpPost]
        public ActionResult DownloadCalculatedStockReport()
        {
            var stockSettings = entities.StockSetting.ToList();
            var products = entities.Product.ToList();
            var categories = entities.Category.ToList();
            var branches = entities.Branch.ToList();

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;
            //Header of table  
            //  
            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;
            workSheet.Cells[1, 1].Value = "Branch";
            workSheet.Cells[1, 2].Value = "Barcode";
            workSheet.Cells[1, 3].Value = "Category";
            workSheet.Cells[1, 4].Value = "Name";
            workSheet.Cells[1, 5].Value = "XS";
            workSheet.Cells[1, 6].Value = "S";
            workSheet.Cells[1, 7].Value = "M";
            workSheet.Cells[1, 8].Value = "L";
            workSheet.Cells[1, 9].Value = "XL";
            workSheet.Cells[1, 10].Value = "XXL";
            //Body of table  
            //  
            int recordIndex = 2;
            foreach (var stockSetting in stockSettings)
            {
                var product = products.FirstOrDefault(p => p.ProductID == stockSetting.ProductID);
                if (product == null) continue;
                var category = categories.FirstOrDefault(c => c.CategoryID == product.CategoryID);
                if (category == null) continue;
                var branch = branches.FirstOrDefault(b => b.BranchID == product.BranchID);
                if (branch == null) continue;

                var amountXS = product.AmountXS - stockSetting.MinXS > 0 ? 0 : stockSetting.MinXS - product.AmountXS;
                var amountS = product.AmountS - stockSetting.MinS > 0 ? 0 : stockSetting.MinS - product.AmountS;
                var amountM = product.AmountM - stockSetting.MinM > 0 ? 0 : stockSetting.MinM - product.AmountM;
                var amountL = product.AmountL - stockSetting.MinL > 0 ? 0 : stockSetting.MinL - product.AmountL;
                var amountXL = product.AmountXL - stockSetting.MinXL > 0 ? 0 : stockSetting.MinXL - product.AmountXL;
                var amountXXL = product.AmountF - stockSetting.MinXXL > 0 ? 0 : stockSetting.MinXXL - product.AmountF;

                workSheet.Cells[recordIndex, 1].Value = branch.Name;
                workSheet.Cells[recordIndex, 2].Value = product.Barcode;
                workSheet.Cells[recordIndex, 3].Value = category.Name;
                workSheet.Cells[recordIndex, 4].Value = product.Name;
                if (amountXS > 0) workSheet.Cells[recordIndex, 5].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                workSheet.Cells[recordIndex, 5].Value = amountXS;
                if (amountS > 0) workSheet.Cells[recordIndex, 6].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                workSheet.Cells[recordIndex, 6].Value = amountS;
                if (amountM > 0) workSheet.Cells[recordIndex, 7].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                workSheet.Cells[recordIndex, 7].Value = amountM;
                if (amountL > 0) workSheet.Cells[recordIndex, 8].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                workSheet.Cells[recordIndex, 8].Value = amountL;
                if (amountXL > 0) workSheet.Cells[recordIndex, 9].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                workSheet.Cells[recordIndex, 9].Value = amountXL;
                if (amountXXL > 0) workSheet.Cells[recordIndex, 10].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                workSheet.Cells[recordIndex, 10].Value = amountXXL;
                recordIndex++;
            }
            workSheet.Column(1).AutoFit();
            workSheet.Column(2).AutoFit();
            workSheet.Column(3).AutoFit();
            workSheet.Column(4).AutoFit();
            workSheet.Column(5).AutoFit();
            workSheet.Column(6).AutoFit();
            workSheet.Column(7).AutoFit();
            workSheet.Column(8).AutoFit();
            workSheet.Column(9).AutoFit();
            workSheet.Column(10).AutoFit();
            string excelName = "MinimumStockReport-" + DateTime.Now.ToShortDateString();
            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }

            return RedirectToAction("MinimumStockSetting");

        }

        public ActionResult Register()
        {
            ViewBag.ProvinceList = GetProvincesForDDL();
            var cityListStr = new List<string>();
            cityListStr.Add("*กรุณาเลือกจังหวัดก่อน*");
            var citySelList = new SelectList(cityListStr);
            ViewBag.CityList = citySelList;  //entities.City.Where(c => c.ProvinceID == 1).Select(c => new SelectListItem { Text = c.Name, Value = c.CityID.ToString() });

            return View();
        }

        [HttpPost]
        public ActionResult DoRegister(string UserName, string ShopName, string Website, string LineID, string Phone, string Province, string CityList, string Address, string Email, string radioInline)
        {
            var provinceName = string.Empty;
            var cityName = "กรุงเทพมหานคร"; //default
            var provinceID = -1;

            if (Int32.TryParse(Province, out provinceID))
            {
                provinceName = entities.Province.FirstOrDefault(p => p.ProvinceID == provinceID).Name;
            }
            if (!string.IsNullOrEmpty(CityList) && !CityList.StartsWith("*"))
            {
                var cityId = Int32.Parse(CityList);
                cityName = entities.City.FirstOrDefault(c => c.CityID == cityId).Name;
            }
            var user = new User
            {
                UserName = UserName,
                ShopName = ShopName,
                Website = Website,
                LineID = LineID,
                Address = Address,
                Approved = null,
                City = cityName,
                Email = Email,
                Phone = Phone,
                Province = provinceName,
                RegisteredDate = DateTime.Now
            };

            if (radioInline == "dropship")
            {
                user.UserType = 2;
            }
            else if (radioInline == "wholesale1")
            {
                user.UserType = 3;
            }
            else if (radioInline == "wholesale2")
            {
                user.UserType = 4;
            }

            entities.User.Add(user);
            entities.SaveChanges();

            return RedirectToAction("RegisterComplete");
        }

        [HttpPost]
        public ActionResult GetListCity(string provinceId)
        {
            int proId;

            if (!Int32.TryParse(provinceId, out proId))
            {
                proId = 1;
            }

            var cityList = entities.City.Where(c => c.ProvinceID == proId).Select(c => new SelectListItem { Text = c.Name, Value = c.CityID.ToString() }).ToList();

            //var citySelList = new SelectList(cityList, "Value", "Text", 0);

            return Json(cityList);
        }
        public ActionResult RegisterComplete()
        {
            return View();
        }

        [Authorize]
        public int CalculateRemainingStock(string size, int productId)
        {
            var stock = entities.Product.FirstOrDefault(p => p.ProductID == productId);
            var totalOrder = entities.Order.Where(o => o.ProductID == productId).ToList();

            var sumRemainingXS = totalOrder == null ? 0 : totalOrder.Sum(o => o.AmountXS.Value);
            var sumRemainingS = totalOrder == null ? 0 : totalOrder.Sum(o => o.AmountS.Value);
            var sumRemainingM = totalOrder == null ? 0 : totalOrder.Sum(o => o.AmountM.Value);
            var sumRemainingL = totalOrder == null ? 0 : totalOrder.Sum(o => o.AmountL.Value);
            var sumRemainingXL = totalOrder == null ? 0 : totalOrder.Sum(o => o.AmountXL.Value);
            var sumRemainingXXL = totalOrder == null ? 0 : totalOrder.Sum(o => o.AmountF.Value);

            if (stock != null)
            {
                switch (size.ToUpper())
                {
                    case "XS":
                        if (stock.AmountXS != null) return stock.AmountXS.Value - sumRemainingXS;
                        return 0;
                    case "S":
                        if (stock.AmountS != null) return stock.AmountS.Value - sumRemainingS;
                        return 0;
                    case "M":
                        if (stock.AmountM != null) return stock.AmountM.Value - sumRemainingM;
                        return 0;
                    case "L":
                        if (stock.AmountL != null) return stock.AmountL.Value - sumRemainingL;
                        return 0;
                    case "XL":
                        if (stock.AmountXL != null) return stock.AmountXL.Value - sumRemainingXL;
                        return 0;
                    case "XXL":
                        if (stock.AmountF != null) return stock.AmountF.Value - sumRemainingXXL;
                        return 0;
                }
            }

            return 0;
        }

        [Authorize]
        public JsonResult getCategorySizes(string categoryID)
        {
            var catID = int.Parse(categoryID);
            var categoryObj = entities.Category.FirstOrDefault(c => c.CategoryID == catID);

            var sizes = new { xs = categoryObj.XS, s = categoryObj.S, m = categoryObj.M, l = categoryObj.L, xl = categoryObj.XL, xxl = categoryObj.XXL };
            return Json(sizes, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public string GetUserLastOrderDate(string userID)
        {
            if (!string.IsNullOrEmpty(userID))
            {
                var id = int.Parse(userID);
                var lastOrderDate = entities.Order.Where(o => o.UserID == id).Max(o => o.OrderDateTime);

                return lastOrderDate.HasValue ? lastOrderDate.Value.ToShortDateString() : "ยังไม่มีข้อมูล";
            }
            return "ยังไม่มีข้อมูล";
        }

        [Authorize]
        public string GetUserName(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var userId = int.Parse(id);
                return entities.User.FirstOrDefault(u => u.UserID == userId).UserName;

            }
            return "ลูกค้าสั่งเอง";
        }

        public string test()
        {
            return "This is a test.";
        }

        private int GetBranchID(string name)
        {
            var branch = entities.Branch.FirstOrDefault(b => b.Name == name);

            return (branch == null ? 0 : branch.BranchID);
        }
    }
}